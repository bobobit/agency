<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Layout extends MY_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model("page/page_m");
        $this->data['menu'] = $this->page_m->get_public_menu();
    }

    public function index() {
        redirect('/');
    }

    public function _one_page($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('one_page', $data);
    }

    public function _authentication($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('authentication', $data);
    }

    public function _admin($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('admin', $data);
    }

    public function _blog($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('blog', $data);
    }

    public function _image_gallery($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('image_gallery', $data);
    }
    
    public function _healtyyoga($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('healtyyoga', $data);
    }  
      
    public function _softskillstraining($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('softskillstraining', $data);
    }
}