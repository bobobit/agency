<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Googleanalytics extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array(
            'content' => 'renamethis/index'
        );
        $this->load->view('index', $data);
    }

    public function embed(){
        $data = array(
            'content' => 'googleanalytics/embed'
        );
        $this->load->view('embed', $data);
    }

    public function apijsauthentication(){
        $data = array(
            'content' => 'googleanalytics/apijsauthentication'
        );
        $this->load->view('apijsauthentication', $data);
    }

}