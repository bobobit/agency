Carousel = function(){

	var count = 0;
	var index = 0;
	var prev  = 0;
	var curr  = 0;
	var next  = 0;

	return {
		set_count: function(icount){
			count = icount;
		},
		get_count: function(){
			return count;
		},
		set_index: function(iindex){
			index = iindex;
		},
		get_index: function(){
			return index;
		},
		set_prev: function(iprev){
			prev = iprev;
		},
		get_prev: function(){
			return prev;
		},
		set_curr: function(icurr){
			curr = icurr;
		},
		get_curr: function(){
			return curr;
		},
		set_next: function(inext){
			next = inext;
		},
		get_next: function(){
			return next;
		},
	};//end return
};/*end Carousel*/
MyPhotoGalleryLightboxDOM = function(){

	var lightbox_dom = null;
	
	return {
		set_lightbox_dom: function(ilightbox_dom){
			lightbox_dom = ilightbox_dom;
		},
		get_lightbox_dom: function(){
			return lightbox_dom;
		},
	};//end return
};/*end MyPhotoGalleryLightboxDOM*/
$(function(){
/*jQuery*/

// console.log('log');

	var carousel = new Carousel();
	var carouselPhotoGallery = new Carousel();
	var myPhotoGalleryLightboxDOM = new MyPhotoGalleryLightboxDOM();

	/*BLOG SEARCH*/
	$('#blog_search_button').click(function(){
		
		var query = $('#blog_search_query').val();
		var url = $('#blog_search').attr('data-url');

		$('#blog_search').slideUp(function(){
			$.post(url, { blog_search_query: query}, function(data){
				$('#blog_search').html(data);
				$('#blog_search').slideDown();
			});
		});
		
	});

	/*POST SEARCH BY TITLE*/
	$('#post_search_button').click(function(){
		
		var query = $('#post_search_query').val();
		var url = $('#post_search').attr('data-url');

		$('#post_search').slideUp(function(){
			$.post(url, { post_search_query: query}, function(data){
				$('#post_search').html(data);
				$('#post_search').slideDown();
			});
		});
		
	});

	


	/*UPDATE CAROUSEL OBJECT - PREV, CURR, NEXT INDEX*/
	$('#myPhotoGalleryCarousel').on('slid.bs.carousel', function () {
       
		that = $(this);
		var carouselData = $(this).data('bs.carousel');
		var total = $('.carousel-inner .item', this).length;
		var end = total - 1;

		var prev = $(that.find('.active').prev()).index();
		var curr = $(that.find('.active')).index();
		var next = $(that.find('.active').next()).index();

		prev = (prev<0)? end : prev;
		next = (next<0)? 0 : next;

		carouselPhotoGallery.set_count(total);
		carouselPhotoGallery.set_prev(prev);
		carouselPhotoGallery.set_curr(curr);
		carouselPhotoGallery.set_next(next);

    });
    /*ON DOC READY RESIZE CAROUSEL IMAGE - ova e bitno ne go brisi !!!*/
    $('#myPhotoGalleryCarousel .carousel-img-fill', this).css('height','100%');


	/*TRIGGER LIGHT BOX -Carousel Image Preview*/
	$('#myPhotoGalleryCarousel a.lightbox-trigger').on('click', function () {

		var src = $(this).attr('data-images-path') + '/' + $(this).attr('data-photo-url');
		var title = $(this).attr('data-photo-title');
		$('#myPhotoGalleryLightbox img').attr('src', src);
		$('#myPhotoGalleryLightbox div.lightbox-caption p').text(title);

		carouselPhotoGallery.set_index(carouselPhotoGallery.get_curr());

	});
	/*HIDE.BS.LIGHTBOX*/
	$(document).on('hide.bs.lightbox', function(){
		$('#myPhotoGalleryCarousel').carousel(carouselPhotoGallery.get_index());
		$('#myPhotoGalleryLightbox').removeClass('fit');
		// myPhotoGalleryLightboxDOM.set_lightbox_dom(null);
		
	});
	/*SHOW.BS.LIGHTBOX*/
	$(document).on('show.bs.lightbox', function(){
		var lightbox = $('#myPhotoGalleryLightbox');
		/*Ovde morav da napavam osnovni korekcii na lightbox, 
		zatoa sto se pojaviva plav border na right i bottom*/
		lightbox.css({
			'background-color': 'rgba(0,0,0, 0.88)',
			'overflow' : 'hidden',
			'left' : -10,
			'top' : -10,
			'width' : ($(window).width() + 15),
			'height' : ($(window).height() + 15)
		});
		lightbox.find('.lightbox-caption').css({
			'display': 'none'
		});

	});
	/*Resise LIGHTBOX to fit in window on double click*/
	$('#myPhotoGalleryLightbox img').dblclick(function() {

		var lightbox = $('#myPhotoGalleryLightbox');

		if ($(lightbox).hasClass('fit')) {
			/* RESTOTE LIGHTBOX UN-FIT */
			restoreLightbox(myPhotoGalleryLightboxDOM.get_lightbox_dom());
		} else {
			//zacuvajgo prviot lightbox za posle na sledniot doubleclick da mu napravis restore
			var clone = lightbox.clone(true);
			myPhotoGalleryLightboxDOM.set_lightbox_dom(clone);

			/* LIGHTBOX FIT */
			lightboxFit();
		}

	});

	/* BACK TO TOP LINK */
	// Only enable if the document has a long scroll bar
	// Note the window height + offset
	if ( ($(window).height() + 100) < $(document).height() ) {
	    $('#top-link-block').removeClass('hidden').affix({
	        // how far to scroll down before link "slides" into view
	        offset: {top:100}
	    });
	}
	
});/*end document ready*/
















/*-------------------------------------------------------------------------------------------------------*/

function get_image_src(attr_style_url){
	return attr_style_url.substring(4, attr_style_url.length-1); 
}

/* LIGHTBOX FIT IN WINDOW (RESIZE BY HEIGHT)*/
var lightboxFit = function() {

	var that = $('#myPhotoGalleryLightbox');
	var windowHeight,
		windowWidth,
		padTop,
		padBottom,
		padLeft,
		padRight,
		$image,
		preloader,
		originalWidth,
		originalHeight;

	//Postavi klasa fit
	$(that).addClass('fit');//ovaa klasa nema nisto voneze, sluzi samo za toggle fit/unfit na lightbox on double click 

	// Get the window width and height.
	windowHeight = $(window).height();
	windowWidth = $(window).width();
	// Get the top, bottom, right, and left padding
	padTop = parseInt( that.find('.lightbox-content').css('padding-top') , 10);
	padBottom = parseInt( that.find('.lightbox-content').css('padding-bottom') , 10);
	padLeft = parseInt( that.find('.lightbox-content').css('padding-left') , 10);
	padRight = parseInt( that.find('.lightbox-content').css('padding-right') , 10);
	// Load the image, we have to do this because if the image isn't already loaded we get a bad size
	$image = that.find('.lightbox-content').find('img:first');
	if($image.length <= 0) return false;
	preloader = new Image();
	preloader.onload = function() {
		
		// Ako VISINATA na lightboxot e pomala od window h poradi toa so e mala slikata, togas maksimiziraj
		if( (preloader.height + padTop + padBottom) <= windowHeight) {
			originalWidth = preloader.width;
			originalHeight = preloader.height;

			preloader.height = windowHeight - padTop - padBottom;
			preloader.width = originalWidth / originalHeight * preloader.height;
		}

		that.find('.lightbox-dialog').css({
			'position': 'fixed',
			'width': preloader.width + padLeft + padRight,
			'height': preloader.height + padTop + padBottom,
			'top' : (windowHeight / 2) - ( (preloader.height + padTop + padBottom) / 2),
			'left' : '50%',
			'margin-left' : -1 * (preloader.width + padLeft + padRight) / 2
		});

		that.find('.lightbox-content').css({
			'width': preloader.width + padLeft + padRight,
			'height': preloader.height + padTop + padBottom
		});

		$image.css({
			'width': preloader.width,
			'height': preloader.height
		});

		that.find('.lightbox-caption').css({
			'display': 'none'
		});

		that.css({
			'background-color': 'rgba(0,0,0, 0.88)',
			'overflow' : 'hidden'
		});

	};
	preloader.src = $image.attr('src');
};
/* RESTORE LIGHTBOX TO ORIGINAL WINDOW (UN-FIT) */
var restoreLightbox = function(myPhotoGalleryLightboxDOM) {

	var that = $('#myPhotoGalleryLightbox');
	var old = myPhotoGalleryLightboxDOM;
	var width,
		height,
		imgWidth,
		imgHeight,
		top,
		margin_left
	;

	//Izbrisi klasa fit
	$(that).removeClass('fit');

	width 		= parseInt( old.find('.lightbox-dialog').css('width') 		, 10);
	height 		= parseInt( old.find('.lightbox-dialog').css('height') 		, 10);
	top 		= parseInt( old.find('.lightbox-dialog').css('top') 		, 10);
	margin_left = parseInt( old.find('.lightbox-dialog').css('margin-left') , 10);
	
	imgWidth  = parseInt( old.find('.lightbox-content').find('img:first').css('width' ) , 10);
	imgHeight = parseInt( old.find('.lightbox-content').find('img:first').css('height') , 10);

	that.find('.lightbox-dialog').css({
		'position': 'fixed',
		'width': width,
		'height': height,
		'top' : top,
		'left' : '50%',
		'margin-left' : margin_left
	});

	that.find('.lightbox-content').css({
		'width': width,
		'height': height
	});

	that.find('.lightbox-content').find('img:first').css({
		'width': imgWidth,
		'height': imgHeight
	});

	that.find('.lightbox-caption').css({
		'display': 'block'
	});

	that.css({
		'background-color': 'rgba(0,0,0, 0.88)',
		'overflow' : 'hidden'
	});

};