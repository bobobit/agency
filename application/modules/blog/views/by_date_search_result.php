<?php if (!empty($posts)) { ?>
<div class="panel panel-default" id="blog_search_results">
	<div class="panel-heading">
		<small>Choose post from the list</h3>
	</div>
	<div class="panel-body">
		<ul class="list-unstyled">
		<?php foreach ($posts as $post) { ?>
			<li><a target="_parent" href="<?php echo site_url('blog/post/' . $post->id); ?>">
			<span class="glyphicon glyphicon-hand-right"></span>
			<i><?php echo $post->title; ?></i>
			</a></li>
		<?php }/*end foreach*/ ?>
		</ul>
	</div>
</div>
<?php } else { ?>
<div class="alert alert-warning alert-dismissible" role="alert" id="no_results">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <span>No results were found</span>
</div>
<?php } ?>