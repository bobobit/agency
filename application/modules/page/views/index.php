    <?php $language = $this->session->userdata('language'); ?>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">
                <div style="position: relative; margin: 0 auto; max-width: 70%">
                    <?php echo lang('intro_lead_in'); ?>
                </div>
                </div>
                <div class="intro-heading"><?php //echo lang('intro_heading'); ?></div>
                <a href="#services" class="page-scroll btn btn-xl"><?php echo mactoupper(lang('intro_tell_me_more_btn'), $language); ?></a>
            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 ">
                </div>
                <div class="col-lg-10 text-center">
                    <h2 class="section-heading"><?php echo lang('services_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('services_subtitle'); ?></h3>
                </div>
                <div class="col-lg-1 ">
                </div>
            </div>
            <div class="col-sm-1 ">
            </div>
            <div class="col-sm-4 ">
                <div class="service-port">
                    <a href="<?php echo site_url('healtyyoga'); ?>">
                        <img src="<?php echo site_url('assets/img/joga-services.jpg'); ?>" class="img-responsive img-circle" alt="">
                    </a>
                    <h4><?php echo lang('services_healtyyoga_gate_title'); ?></h4>
                    <p class="text-muted"><?php echo lang('services_healtyyoga_gate_para'); ?></p>
                    <a href="<?php echo site_url('healtyyoga'); ?>" class="btn btn-md"><?php echo lang('services_tell_me_more_btn'); ?></a>
                </div>
            </div>
            <div class="col-sm-1 ">
            </div>
            <div class="col-sm-1 ">
            </div>
            <div class="col-sm-4 ">
                <div class="service-port">
                    <a href="<?php echo site_url('softskillstraining'); ?>">
                        <img src="<?php echo site_url('assets/img/soft_skills_training_services.jpg'); ?>" class="img-responsive img-circle" alt="">
                    </a>
                    <h4><?php echo lang('softskills_title'); ?></h4>
                    <p class="text-muted"><?php echo lang('softskills_subtitle'); ?></p>
                    <a href="<?php echo site_url('softskillstraining'); ?>" class="btn btn-md"><?php echo lang('services_tell_me_more_btn'); ?></a>
                </div>
            </div>
        </div>
    </section>

    <!-- Training Grid Section -->
    <?php echo Modules::run('training/training_grid_section'); ?>

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('about_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('about_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="<?php echo site_url('assets/img/BIljana-Peseva-mirror.jpg'); ?>" class="img-responsive img-circle" alt="">
                        <h4><?php echo lang('about_autor_img_title'); ?></h4>
                        <p class="text-muted"><?php echo lang('about_autor_img_subtitle'); ?></p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="biography-panel">
                        <div class="biography-heading">
                            <h4><?php echo mactoupper('Biljana Pe{eva', $language); ?></h4>
                            <h4 class="subheading"></h4>
                        </div>
                        <div class="biography-body">
                            <p class="text-muted">
                                M-r Biljana Pe{eva,  e profesionalen obu~uva~ na vozrasni po verifikuvana programa na Ministerstvoto za Obrazovanie, osnova~ i voditel na obuki i ve`bi  za zajaknuvawe na timskiot i li~niot potencijal.
                            </p>
                            <p class="text-muted">
                                Biljana e diplomiran ekonomist na Ekonomski Fakultet vo Skopje i ima steknato  dokvalifikacija za pedagogija i metodologija od Filozofski fakultet  vo Skopje. Magistrira vo 2007 godina na Ekonomski Fakultet oblast Marketing. Vo svoeto rabotno iskustvo ima raboteno vo razli~ni organizacii vo razvivawe, realizacija  i/ili u~estvuvano,  vo proekti za odr`liv razvoj, psiho-socijalni proekti i proekti za razvoj na novi sovremeni tehnologii  oblast samouslu`ni produkti i kanali. 
                            </p>
                            <p class="text-muted">
                                Od neformalno obrazovanie ima dobieno kvalifikacija od oblast Transakciska Analiza, organizacisko rabotewe, rabotewe vo tim i grupi, upravuvawe so stres i steknato nacionalen serifikat za Komunikacija i komunikaciski ve{tini od verifikuvana programa vo Ministerstvo za Obrazovanie na Republika Makedonija.  
                            </p>
                            <p class="text-muted">
                                Biljana e dolgogodi{en praktikant i prou~uva~ na klasi~na Joga, meditacija, i tehniki za di{ewe, spored programite na Biharskata {kola na Jogata i [ri [ri Joga na me|unarodnata fondacija Umetnost na `iveewe.  Ima u~estvuvano i vodeno seminari, obuki i programi za joga i tehniki za namaluvawe na stresot kaj moderniot ~ovek.
                            </p>
                            <p class="text-muted">
                                Vo momentot vodi obuki, ve`bi i rabotilnici na vozrasni i toa: Joga ve`bi za podobar fizi~ki i mentalen  razvoj i dr`i Obuki i seminari za meki ve{tini na rabotni timovi.
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('history_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('history_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo site_url('assets/img/SUNA-logo-services.jpg'); ?>" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>2014/2015 PRVA GENERACIJA</h5>
                                    <h4>MODUS CENTAR</h4>
                                    <h4 class="subheading">Konsultativno-edukativen centar</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">
                                        Nacionalno priznat sertifikat za profesionalen obu~uva~ na vozrasni od Ministerstvo za Obrazovanie soglasno Zakonot za Obrazovanie na Vozrasni vo Republika Makedonija nacionalno priznat sertifikat za pominato programa Komunikacija i komunikaciski ve{tini 500  ~asa napredna edukacija preku referentna ramka Transakciska Analiza (Razvoj na komunikaciski ve{tini i efikasnost vo delovna sredina, tehniki i fazi vo pregovarawe, razre{uvawe i prevenirawe na konflikti)
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo site_url('assets/img/yoga-sandrata-history.jpg'); ?>" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>2008-2015</h5>
                                    <h4>UMETNOST NA @IVEEWE</h4>
                                    <h4 class="subheading">u~itel za rabotilnici Voda Zdiv i Vozduh</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="<?php echo site_url('assets/img/Think-Outside-the-Box-history.jpg'); ?>" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h5>2013</h5>
                                    <h4 style="font-family: serif">TLEX</h4>
                                    <h5 style="font-family: serif">TRANSFORMATION LEDERSHIP TOWARDS EXELENCE </h5>
                                    <h4 class="subheading">Trening za komunikacija i marketing </h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Bidi del
                                    <br>od na{ata
                                    <br>prikazna!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('contact_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('contact_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" data-language="<?php echo $language; ?>" data-success-alert="<?php echo lang('contact_form_success_alert'); ?>" data-warning-alert="<?php echo lang('contact_form_warning_alert'); ?>" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="<?php echo mactoupper(lang('contact_input_name'), $language); ?>" id="name" required data-validation-required-message="<?php echo lang('contact_error_data_validation_name'); ?>">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="<?php echo mactoupper(lang('contact_input_email'), $language); ?>" id="email" required data-validation-required-message="<?php echo lang('contact_error_data_validation_email'); ?>">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-6">
                                            <?php echo Modules::run('captcha'); ?>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-6">
                                            <input type="text" class="form-control" placeholder="<?php echo mactoupper(lang('contact_input_captcha'), $language); ?>" id="captcha" required data-validation-required-message="<?php echo lang('contact_error_data_validation_captcha'); ?>">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="<?php echo mactoupper(lang('contact_input_message'), $language); ?>" id="message" required data-validation-required-message="<?php echo lang('contact_error_data_validation_message'); ?>"></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>


                            <div class="clearfix"></div>


                            
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl"><?php echo lang('contact_send_message_btn'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>