<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding: 33px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">


<h1><?php echo lang('signup_user_heading');?></h1>
<p><?php echo lang('signup_user_subheading');?></p>

<?php if($message != false){ ?>
<div id="infoMessage" class="alert alert-warning">
<?php echo $message;?>
</div>
<?php } ?>


<?php echo form_open("client/signup", array('role' => 'form', 'id' => 'signup_user_form'));?>
      <div class="form-group">
            <label class="control-label" for="<?php echo $first_name['name']; ?>">
            <?php echo lang('signup_user_fname_label', 'first_name');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $first_name['name'], 
                  'type'      =>    $first_name['type'], 
                  'id'        =>    $first_name['id'], 
                  'value'     =>    $first_name['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $last_name['name']; ?>">
            <?php echo lang('signup_user_lname_label', 'last_name');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $last_name['name'], 
                  'type'      =>    $last_name['type'], 
                  'id'        =>    $last_name['id'], 
                  'value'     =>    $last_name['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $company['name']; ?>">
            <?php echo lang('signup_user_company_label', 'company');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $company['name'], 
                  'type'      =>    $company['type'], 
                  'id'        =>    $company['id'], 
                  'value'     =>    $company['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $email['name']; ?>">
            <?php echo lang('signup_user_email_label', 'email');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $email['name'], 
                  'type'      =>    $email['type'], 
                  'id'        =>    $email['id'], 
                  'value'     =>    $email['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $phone['name']; ?>">
            <?php echo lang('signup_user_phone_label', 'phone');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $phone['name'], 
                  'type'      =>    $phone['type'], 
                  'id'        =>    $phone['id'], 
                  'value'     =>    $phone['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $password['name']; ?>">
            <?php echo lang('signup_user_password_label', 'password');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $password['name'], 
                  'type'      =>    $password['type'], 
                  'id'        =>    $password['id'], 
                  'value'     =>    $password['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $password_confirm['name']; ?>">
            <?php echo lang('signup_user_password_confirm_label', 'password_confirm');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $password_confirm['name'], 
                  'type'      =>    $password_confirm['type'], 
                  'id'        =>    $password_confirm['id'], 
                  'value'     =>    $password_confirm['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>

      <div class="form-group">
            <?php echo form_submit(array(
                    'type'        => 'submit',
                    'name'        => 'submit',
                    'class'       => 'btn btn-primary',
                    'id'          => 'submit',
                    'value'       => lang('signup_user_submit_btn'),
                  ));
            ?>

            <a class="btn btn-default" href="<?php echo site_url('/'); ?>">cancel</a>

      </div>


      
<?php echo form_close();?>


            </div>
        </div>
    </div>
</div>