<?php $this->load->view('partials/image_gallery_header', $this->data); ?>
<?php $this->load->view('partials/image_gallery_navigation', $this->data); ?>

<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

<?php $this->load->view('partials/image_gallery_footer'); ?>