<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Captcha extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('captcha_lib');
        $this->data['captcha'] = $this->captcha_lib->captcha_actions();
    }

    public function index()
    {
        $captcha = $this->data['captcha'];
        $data['captcha_image'] = $captcha['image'];
        $data['captcha_encrypt'] = $captcha['encrypt'];
        $this->load->view('index', $data);
    }

}