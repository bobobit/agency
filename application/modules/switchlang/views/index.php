<?php 
	$sess_lang = $this->session->userdata('language');
	$switchlang = config_item('switchlang');
	$sw = $switchlang[$sess_lang];

	if ($sw == 'english') {
		$font_family = 'Helvetica, Arial, sans-serif';
	}
	if ($sw == 'macedonian') {
		$font_family = 'Macedonian, Helvetica, Arial, sans-serif';
	}

	$translatelang = config_item('translatelang');
	$tr = $translatelang[$sw];

	$language = substr($tr, 0, 3);
	// $language = $tr;

?>

<a href="<?php echo site_url('switchlang/sw/' . $this->uri->uri_string()); ?>"  style="font-family: <?php echo $font_family; ?>">
	<small class="btn-group btn-group-xs">
		<i class="badge">
			<?php echo $language; ?>
		</i>
	</small>
</a>
