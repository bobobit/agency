    <!-- Training Modals -->
    <!-- Use the modals below to showcase details about your training trainings! -->

<?php foreach ($trainings as $training) { ?>

    <!-- Training Modal No -->
    <div class="training-modal modal fade" id="trainingModal<?php echo $training->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo mactoupper($training->title, $language); ?></h2>
                            <p class="item-intro text-muted"><?php echo $training->subtitle; ?></p>
                            
                            <div class="content clearfix">
                                <img class="img-responsive pull-left" src="<?php echo site_url('assets/img/' . $training->img_url); ?>" alt="">
                                <?php echo $training->contents; ?>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="modal-body">
                            <p><button type="button" class="btn btn-primary close-button" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php }/*end for each trainings*/ ?>