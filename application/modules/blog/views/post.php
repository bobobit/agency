<?php $pst = $posts;/*ovde se misli na eden post, imeto na promenlivata ostana vo mnozina od prethodno*/ ?>
<div class="row">
	<div class="row col-lg-12">
		<h3 class="clearfix">
			<div class="col-lg-7 pull-left">
				<big>
					<?php echo $pst->title; ?>
				</big>
			</div>
			<div class="col-lg-5 pull-right">
				<small>
					<span class="glyphicon glyphicon-time"></span> 
					Posted on 
					<?php echo date("j F Y, g:i a", strtotime($pst->created)); ?>
				</small>
			</div>
		</h3>
	</div>
	<div class="col-lg-12">
	<hr>
	</div>
	<article class="col-lg-12">
		<?php echo add_base_url_to_img_path($pst->body, site_url('assets/img')) ?>
	</article>


	<!-- COMMENTS -->
	<div class="col-lg-12">
		<h3>Comments (<?php echo $comments_count; ?>)</h3>
	</div>	
	<div class="col-lg-12">
		<hr>
		
	</div>
	<?php foreach ($comments as $key => $comment) { ?>
	<div class="col-lg-12 ">
		<div class="media">
            <a class="pull-left" href="#">
                <img class="media-object" src="<?php echo site_url('assets/img/placeholders/64x64.jpg'); ?>" alt="">
            </a>
            <div class="media-body">
                <h4 class="media-heading">
					<span><b><?php echo $comment->commentator; ?></b></span>
					
			        <small><span class="glyphicon glyphicon-time"></span></small>
					<span><small><?php echo $comment->created; ?></small></span>
                </h4>
                <p>
					<?php echo $comment->comment; ?>
				</p>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
		<hr>
		
	</div>
	<?php }/*end foreach comments*/ ?>


	<!-- COMMENTS FORM -->
	<div class="col-lg-12">
		<h2>Leave a comment</h2>
		<p>Your email address will not be shared or published. Required fields are marked <i class="glyphicon glyphicon-ok"></i></p>
	</div>

	<div class="col-lg-12">
  		<form role="form" action="<?php echo site_url('blog/post/' . $pst->id); ?>" method="post" >
<?php /*Ako imame logiran user togas ovie podatoci gi vlecime od tamu*/ ?>
<?php if (empty($user)) { ?>
	      <div class="form-group">
	        <label for="name">Name</label>
	        <div class="input-group">
	          <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $this->input->post('name'); ?>" required>
	          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
	        </div>
	      </div>   

	      <div class="form-group">
	        <label for="email">E-Mail</label>
	        <div class="input-group">
	          <input type="text" class="form-control" name="email" id="email" placeholder="Enter E-Mail" value="<?php echo $this->input->post('email'); ?>" required>
	          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
	        </div>
	      </div>

	      <div class="form-group">
	        <label for="website">Website</label>
	        <div class="input-group">
	          <input type="text" class="form-control" name="website" id="website" placeholder="Enter Website" value="<?php echo $this->input->post('website'); ?>">
	          <span class="input-group-addon"><i class="glyphicon glyphicon-minus form-control-feedback"></i></span>
	        </div>
	      </div>
<?php }/*end if user logged in*/ ?>

	      <div class="form-group">
		    <label for="comment">Comment</label>
		    <div class="input-group">
		    	<textarea name="comment" id="comment" class="form-control" rows="10" cols="130"></textarea>
		    </div>
		    <br>
		    <div class="input-group">
		    	<input type="submit" name="submit" id="submit" value="Post Comment" class="btn btn-info pull-left">
		    </div>
		    <hr class="featurette-divider hidden-lg">
		  </div>
      	</form>
	</div>



</div>



