<!DOCTYPE html>
<html>
<head>
	<title>Font Chooser</title>
	<?php $language= '-mac';  ?>
	<link href="<?php echo site_url('assets/css/agency-sass' . $language . '.css'); ?>" rel="stylesheet">
</head>
<body>

<h1> Makedonski Fontovi </h1>

<?php foreach ($font_family as $key => $ff) { ?>
<br>
<h3 style='font-family: "Helvetica Arial sans-serif"'>Font <?php echo $key; ?>: <?php echo $ff; ?></h3>
<div style='font-family: "<?php echo $ff; ?>"'>
	<?php echo $content->lorem; ?>
</div>

<br>
<div>

	>
	<span style='font-family: "Helvetica Arial sans-serif"'>Unformatted: </span>
	<span style='font-family: "<?php echo $ff; ?>";'><?php echo $content->neformatiran; ?></span>
	>
	<span style='font-family: "Helvetica Arial sans-serif"'>Bold: </span>
	<span style='font-family: "<?php echo $ff; ?>"; font-weight: bold;'><?php echo $content->bold; ?></span>
	>
	<span style='font-family: "Helvetica Arial sans-serif"'>Italik: </span>
	<span style='font-family: "<?php echo $ff; ?>"; font-style: italic;'><?php echo $content->italic; ?></span>
	>
	<span style='font-family: "Helvetica Arial sans-serif"'>Bold-Italik: </span>
	<span style='font-family: "<?php echo $ff; ?>"; font-weight: bold; font-style: italic;'><?php echo $content->bolditalic; ?></span>

</div>

<div>
	<span style='font-family: "Helvetica Arial sans-serif"'>Sizes: </span>
	...
	(
	<span style='font-family: "Helvetica Arial sans-serif"'>1em: </span>
	-
	<span style='font-family: "<?php echo $ff; ?>"; font-style: italic; font-size: 1em; '>golemina na font</span>
	)
	...
	(
	<span style='font-family: "Helvetica Arial sans-serif"'>1.5em: </span>
	-
	<span style='font-family: "<?php echo $ff; ?>"; font-style: italic; font-size: 1.5em; '>golemina na font</span>
	)
	...
	(
	<span style='font-family: "Helvetica Arial sans-serif"'>2em: </span>
	-
	<span style='font-family: "<?php echo $ff; ?>"; font-style: italic; font-size: 2em; '>golemina na font</span>
	)
</div>

<br>
<div>

	<p>Azbuka verzija1</p>
	<p style='font-size: 1.2em; font-family: "<?php echo $ff; ?>";'><?php echo $content->azbuka_v1; ?></p>
	<p style='font-size: 1.2em; font-family: "Helvetica Arial sans-serif"'><?php echo $content->azbuka_v1; ?></p>
	
	<p>Azbuka verzija2</p>
	<p style='font-size: 1.2em; font-family: "<?php echo $ff; ?>";'><?php echo $content->azbuka_v2; ?></p>
	<p style='font-size: 1.2em; font-family: "Helvetica Arial sans-serif"'><?php echo $content->azbuka_v2; ?></p>
	
</div>
<hr>
<?php } ?>



</body>
</html>
