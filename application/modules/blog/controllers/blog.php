<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Blog extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->data['scroll_to'] = 'blog';

        // Load Blog Language
        $this->lang->load('blog');
        // $this->lang->load('calendar');

    }

    public function index()
    {
        $this->post();
    }

    public function post($id=NULL) {
        
        if (!$id) {
            /*Ako ne se isprati id togas prikazi gi site postovi*/
            redirect(site_url('blog/posts'));
            return;
        }

        $post = $this->input->post();

        if (!empty($post) && $this->input->post('submit') == 'Post Comment') {
            
            if ($this->_add_comment($id)) {
                /*Ako se izvrsi dodavanjeto na nov komentar 
                togas redirektiraj za da se procita noviot komentar*/
                redirect(site_url('blog/post/' . $id));
            }
        }

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'blog';

        $this->load->model('blog/post_m');
                
        $p = $this->post_m->get_by_id($id);
        if (count($p)<1) {
            /*Ako se isprati nepostoecko id togas prikazi gi site postovi*/
            redirect(site_url('blog/posts'));
            return;
        }
        $post_parts = $this->_post_parts($p);
        $photos = $post_parts['photos'];
        $body_parts = $post_parts['body_parts'];
        $data['subview_data']->photos = $photos;
        $data['subview_data']->body_parts = $body_parts;
        $data['subview_data']->posts = $p;

        $c = $this->_comments($id);
        $data['subview_data']->comments_count = count($c);
        $data['subview_data']->comments = $c;

        $this->load->model('user/user_m');
        $user = $this->user_m->current_user();
        $data['subview_data']->user = $user;

        $this->_render_page('post', $data);

    }

    public function posts(){

        $this->load->library('pagination');
        $this->load->library('paginate_posts');

        $this->load->model('blog/post_m');

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'blog';

        $posts = $this->post_m->get();
        if (sizeof($posts)) {
            $total_posts = count($posts);
            $posts_base_url = base_url('blog/posts');
            $posts_pagination_params = array(
                'base_url' => $posts_base_url, 
                'total_rows' => $total_posts
            );

            $this->paginate_posts->initialize_pagination($posts_pagination_params);
            $data['subview_data']->pagination_links = $this->paginate_posts->create_links();

            $paginated_posts = $this->post_m->get_paginated_posts(
                $this->uri->segment(3), 
                $this->paginate_posts->get_per_page()
            );

            foreach ($paginated_posts as $post) {
                $pbp = $this->_post_body_parts($post);
                $body = array();
                foreach ($pbp as $part) {
                    $body[] = $part;
                }
                $post->body = implode(' ', $body);
            }

            $data['subview_data']->posts = $paginated_posts;
            // ppr($total_posts);
        }/*end if posts*/

        $this->_render_page('posts', $data);

    }/*end fun posts()*/

    public function postsbycategory($category_id=0){

        $this->load->model('blog/post_m');
        $this->load->model('category_m');

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'blog';
        $data['subview_data']->pagination_links = '';

        $posts = $this->post_m->get_by_category_id($category_id);

        foreach ($posts as $post) {
            $pbp = $this->_post_body_parts($post);
            $body = array();
            foreach ($pbp as $part) {
                $body[] = $part;
            }
            $post->body = implode(' ', $body);
            $post->category = $this->category_m->get_by_id($category_id);
            $post->id = $post->post_id;//ova e zaradi join vo modelot, go zema id od post_category tabelata
        }

        $data['subview_data']->posts = $posts;

        $this->_render_page('posts', $data);
    }

    private function _post_body_parts($post){

        $regexp = '/\<img alt=\"\d\d?\" src=\"\w{5}-\w{1,}.jpg" \/>/';
        /*Najdi go tagot img i tuka napravi split na postot*/
        $body_parts = preg_split ($regexp , $post->body);

        return $body_parts;

    }

    private function _post_photos($post){

        $photos = array();

        /*Najdi gi img tagovite i sodrzinata vo niv, i zacuvajgi vo niza img_tags*/
        $regexp = '/\<img alt=\"\d\d?\" src=\"\w{5}-\w{1,}.jpg" \/>/';
        $img_tags = array();
        preg_match_all($regexp , $post->body,  $img_tags);

        $this->load->model("photo/photo_m");
        /*Od najdenite img izvlecigo url-to na slikata*/
        foreach ($img_tags[0] as $m) {
            $reg_url = '/\w{5}-\w{1,}.jpg/';
            preg_match ($reg_url, $m, $url);
            $photos[] = $this->photo_m->get_by_url($url[0]);
        }

        return $photos;

    }

    private function _post_parts($post){

        $body_parts = $this->_post_body_parts($post);
        $photos = $this->_post_photos($post);

        return array(
            'body_parts' => $body_parts,
            'photos' => $photos
            );
    }

    /* RIGHT SIDEBAR*/
    private function _right_sidebar_data($post_id){

        $this->load->model('blog/post_m');
        $this->load->model('blog/post_comment_m');

        $right_sidebar = new stdClass();
        $right_sidebar->recent_posts = $this->post_m->get_recent(5);  
        $right_sidebar->recent_comments = $this->post_comment_m->get_recent(5);
        $right_sidebar->categories = $this->_categories($post_id);

        return $right_sidebar;
    }


    private function _comments($post_id=NULL) {

        $this->load->model('blog/post_comment_m');
        $comments = $this->post_comment_m->get_by_post_id($post_id);

        return $comments;

    }

    private function _add_comment($post_id=NULL) {

            $this->load->model('blog/post_comment_m');
            $this->load->model('user/user_m');
            $user = $this->user_m->current_user();

            $post = $this->input->post();
            if (isset($post['name'])) {
                $data['commentator'] = $post['name'];
            } else {
                $data['commentator'] = $user->username;
            }
            if (isset($post['email'])) {
                $data['commentator_email'] = $post['email'];
            } else {
                $data['commentator_email'] = $user->email;
            }
            
            $data['comment'] = $post['comment'];
            $data['created'] = now('Europe/Skopje');
            $data['post_id'] = $post_id;

            if ($this->post_comment_m->save($data) > 0) {
                return true;
            }

        return false;

    }


    public function blog_search_ajax(){

        $ajax_post = $this->input->post();
        $blog_search_query = $ajax_post['blog_search_query'];

        $this->load->model('blog/post_m');
        $posts = $this->post_m->get_post_like($blog_search_query);

        $data = array(
            'posts' => $posts,
            );

        $this->load->view('post_search_ajax', $data);

    }

    public function post_search_ajax(){

        $ajax_post = $this->input->post();
        $post_search_query = $ajax_post['post_search_query'];

        $this->load->model('blog/post_m');
        $posts = $this->post_m->get_title_like($post_search_query);

        $data = array(
            'posts' => $posts,
            );

        $this->load->view('post_search_ajax', $data);

    }

    public function posts_by_date_ajax(){

        $ajax_post = $this->input->post();
        $search_query = $ajax_post['search_query'];

        $search_date = date_parse($search_query);
        $created = sprintf('%4d-%02d', $search_date['year'], $search_date['month']);

        $this->load->model('blog/post_m');
        $posts = $this->post_m->get_date_like($created);

        $data = array(
            'posts' => $posts,
            );

        $this->load->view('by_date_search_result', $data);

    }

    private function _categories($post_id=null){

        $this->load->model('category_m');
        if($post_id){
            $categories = $this->category_m->get_by_post_id($post_id);
        } else {
            $categories = $this->category_m->get();
        }

        return $categories;
    }



    /*RENDER BLOG PAGE*/
    function _render_page($view, $data=null) {

        $data['module'] = get_class($this);
        $data['subview'] = $view; 

        $post_id = (is_array($data['subview_data']->posts))? null : $data['subview_data']->posts->id;

        $data['sidebar'] = 'right_sidebar';
        $data['sidebar_data'] = $this->_right_sidebar_data($post_id);

        $layout = $data['subview_data']->layout;

        /*Ovde nema so da scroluva ama go zadrzav da mi bidi isto kako page kontrolerot*/
        $data['scroll_to'] = $this->data['scroll_to'];

        echo Modules::run('layout/_'.$layout, $data);

    }

}