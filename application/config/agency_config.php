<?php
/*
|--------------------------------------------------------------------------
| Site Name
|--------------------------------------------------------------------------
| Name of your web site.
| 
*/
$config['site_name'] = 'SUNA';

/*
|--------------------------------------------------------------------------
| Meta Data
|--------------------------------------------------------------------------
| Meta Title is the title of a web page, you can see title in browser tab. It helps both users and search engines.
| Meta Description is a short and precise description of the content of a page. It's longer than meta title. It helps search engine greatly.
| Meta Author
| Meta Keywords
| 
*/
$config['meta_title'] = 'SUNA - Association for development of United Nations values in Macedonia';
$config['meta_description'] = 'Healthy and innovative approaches for Individual and Organizational Transformation';
$config['meta_author'] = 'Biljana Pesheva';
$config['meta_keywords'] = 'Yoga SUNA Skopje Republic of Macedonia';


/*
|--------------------------------------------------------------------------
| Languages
|--------------------------------------------------------------------------
| 
*/
$config['languages'] = array('english', 'macedonian');

$config['switchlang'] = array(
	'english' => 'macedonian',
	'macedonian' => 'english'
);

$config['translatelang'] = array(
	'english' => 'English',
	'macedonian' => 'Makedonski'
);