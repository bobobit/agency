<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding: 33px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10">


<h1><?php echo lang('edit_group_heading');?></h1>
<p><?php echo lang('edit_group_subheading');?></p>

<?php if($message != false){ ?>
<div id="infoMessage" class="alert alert-warning">
<?php echo $message;?>
</div>
<?php } ?>

<?php echo form_open(uri_string(), array('role' => 'form', 'id' => 'edit_group_form'));?>
      <div class="form-group">
            <label class="control-label" for="<?php echo $group_name['name']; ?>">
            <?php echo lang('edit_group_name_label', 'group_name');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $group_name['name'], 
                  'type'      =>    $group_name['type'], 
                  'id'        =>    $group_name['id'], 
                  'value'     =>    $group_name['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $group_description['name']; ?>">
            <?php echo lang('edit_group_desc_label', 'description');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $group_description['name'], 
                  'type'      =>    $group_description['type'], 
                  'id'        =>    $group_description['id'], 
                  'value'     =>    $group_description['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>

      <div class="form-group">
            <?php echo form_submit(array(
                    'type'        => 'submit',
                    'name'        => 'submit',
                    'class'       => 'btn btn-primary',
                    'id'          => 'submit',
                    'value'       => lang('edit_group_submit_btn'),
                  ));
            ?>
            
            <a class="btn btn-default" href="<?php echo site_url('user/users'); ?>">cancel</a>

      </div>

<?php echo form_close();?>


            </div>
        </div>
    </div>
</div>