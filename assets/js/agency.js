/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        var feature_selector = $anchor.attr('href');
        var feature = $(feature_selector);
        
        if (feature.length) {
            $('html, body').stop().animate({
                scrollTop: feature.offset().top
            }, 1500, 'easeInOutExpo');
        }
        event.preventDefault();

        // DATA-SCROLL-TO body attr zacuvuva koj menu item e aktiven
        $('body').attr('data-scroll-to', feature_selector.substring(1));
    });


    // SWITCH LANG AJAX
    $('#switchlang_one_page').click(function(event){
        event.preventDefault();

        var site_url = $(this).attr('data-site-url');
        var ajax_method_url = site_url + 'switchlang/swlang_ajax';
        var scroll_to = $('body').attr('data-scroll-to');
        scroll_to = (scroll_to == 'page-top')? '' : scroll_to;

        $.post( ajax_method_url )
            .done(function( data ) {
                var url = site_url + scroll_to;
                $(location).attr('href', url);
            });

    });

    // So vlez na one-page ako e definiran data-scroll-to togas scrolluvaj do toj del od stranata
    scroll_on_feature();

});// END DOC READY





// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});


/*So vlez va one-page stranata scrolluvaj do opredelen segment*/
function scroll_on_feature(){

    var feature_selector = '#' + $('body').attr('data-scroll-to');
	var feature = $(feature_selector);

    if (feature.length) {
        $('html, body').stop().animate({
            scrollTop: feature.offset().top
        }, 1500, 'easeInOutExpo');
    };

}/*end fun scroll_on_feature()*/
