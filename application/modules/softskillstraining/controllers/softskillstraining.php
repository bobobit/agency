<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Softskillstraining extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['scroll_to'] = 'softskillstraining';
    }

    public function index()
    {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'softskillstraining';

        
        $this->_render_page('index', $data);
    }


    /*RENDER HEALTY YOGA PAGE*/
    function _render_page($view, $data=null) {

        $data['module'] = get_class($this);
        $data['subview'] = $view; 

        $layout = $data['subview_data']->layout;

        /*Ovde nema so da scroluva ama go zadrzav da mi bidi isto kako page kontrolerot*/
        $data['scroll_to'] = $this->data['scroll_to'];

        echo Modules::run('layout/_'.$layout, $data);

    }

}