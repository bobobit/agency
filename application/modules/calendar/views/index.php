<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Datepicker - Only Month Year</title>

		<link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">

		<link href="<?php echo site_url('assets/widgets/datepicker-jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
		<script src="<?php echo site_url('assets/widgets/datepicker-jquery-ui/external/jquery/jquery.js'); ?>"></script>
		<script src="<?php echo site_url('assets/widgets/datepicker-jquery-ui/jquery-ui.js'); ?>"></script>

		<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>

		<style>

			body {
				background-color: rgba(127,127,127, 0.001);
			}

			input#datepicker {
				border-radius: 3px;
				display: block;
				width: 100%;
			}

			div.ui-datepicker {
				font-size:13.2px;
			}

			/* SPINNER */
			.spinner {
			  width: 100px;
			}

			.spinner input {
			  text-align: right;
			}

			.input-group-btn-vertical {
			  position: relative;
			  white-space: nowrap;
			  width: 1%;
			  vertical-align: middle;
			  display: table-cell;
			}

			.input-group-btn-vertical > .btn {
			  display: block;
			  float: none;
			  width: 100%;
			  max-width: 100%;
			  padding: 8px;
			  margin-left: -1px;
			  position: relative;
			  border-radius: 0;
			}

			.input-group-btn-vertical > .btn:first-child {
			  border-top-right-radius: 4px;
			}

			.input-group-btn-vertical > .btn:last-child {
			  margin-top: -2px;
			  border-bottom-right-radius: 4px;
			}

			.input-group-btn-vertical i {
			  position: absolute;
			  top: 0;
			  left: 2px;
			}


		</style>
		<script>
			$(function() {
				var dp = $( ".datepicker" );

				dp.datepicker( { 
					onSelect: function(){
						var date = dp.datepicker( "getDate" );
						$( "#date" ).text(date);
					}
				} );
				
			});/*end doc ready*/
		</script>
	</head>
	<body>

		<div class="">
			<div class="datepicker"></div>
			<div id="date">Date</div>
		</div>

		<div class="container">
		  <div class="input-group spinner">
		    <input type="text" class="form-control" value="42">
		    <div class="input-group-btn-vertical">
		      <button class="btn btn-default"><i class="glyphicon glyphicon-chevron-up"></i></button>
		      <button class="btn btn-default"><i class="glyphicon glyphicon-chevron-down"></i></button>
		    </div>
		  </div>
		</div>
		







<script>

	$(function() {

		/* SPINNER */
		$('.spinner .btn:first-of-type').on('click', function() {
			$('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
		});
		$('.spinner .btn:last-of-type').on('click', function() {
			$('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
		});
		
	});/*end doc ready*/

</script>




	</body>
</html>