<?php

$lang['cal_su']			= "Ne";
$lang['cal_mo']			= "Po";
$lang['cal_tu']			= "Vt";
$lang['cal_we']			= "Sr";
$lang['cal_th']			= "~e";
$lang['cal_fr']			= "Pe";
$lang['cal_sa']			= "Sa";
$lang['cal_sun']		= "Ned";
$lang['cal_mon']		= "Pon";
$lang['cal_tue']		= "Vto";
$lang['cal_wed']		= "Sre";
$lang['cal_thu']		= "~et";
$lang['cal_fri']		= "Pet";
$lang['cal_sat']		= "Sab";
$lang['cal_sunday']		= "Nedela";
$lang['cal_monday']		= "Ponedelnik";
$lang['cal_tuesday']	= "Vrornik";
$lang['cal_wednesday']	= "Sreda";
$lang['cal_thursday']	= "~etvrtok";
$lang['cal_friday']		= "Petok";
$lang['cal_saturday']	= "Sabota";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "Maj";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Avg";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Okt";
$lang['cal_nov']		= "Noe";
$lang['cal_dec']		= "Dek";
$lang['cal_january']	= "Januari";
$lang['cal_february']	= "Februari";
$lang['cal_march']		= "Mart";
$lang['cal_april']		= "April";
$lang['cal_mayl']		= "Maj";
$lang['cal_june']		= "Juni";
$lang['cal_july']		= "Juli";
$lang['cal_august']		= "Avgust";
$lang['cal_september']	= "Septemvri";
$lang['cal_october']	= "Oktomvri";
$lang['cal_november']	= "Novemvri";
$lang['cal_december']	= "Dekemvri";


/* End of file calendar_lang.php */
/* Location: language/macedonian/calendar_lang.php */