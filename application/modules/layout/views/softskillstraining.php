<?php $this->load->view('partials/softskillstraining_header', $this->data); ?>
<?php $this->load->view('partials/softskillstraining_navigation', $this->data); ?>

<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

<?php $this->load->view('partials/softskillstraining_footer'); ?>