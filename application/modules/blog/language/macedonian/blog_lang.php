<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['blog_search_title']				= 'Baraj Post';
$lang['blog_post_categories_title']		= 'Post Kategorii';
$lang['blog_recent_posts_title']		= 'Posledni Postovi';
$lang['blog_posts_by_title_title']		= 'Postovi po Naslov';
$lang['blog_posts_by_date_title']		= 'Postovi po Datum';
$lang['blog_recent_comments_title']		= 'Posledni Komentari';
$lang['blog_post_search_no_result']		= 'Nema Rezultati';
