<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('encrypt');

		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->model('temp_users_m');

	}

	function index()
	{
		$this->login();
	}	

    //signup a user
    public function signup() {

        $this->lang->load('user/auth');
        $this->load->model('client/client_m');
        $this->load->library('form_validation');

        $this->data['title'] = "Sign Up - User Registration";

        if ($this->ion_auth->logged_in())
        {
            redirect('/', 'refresh');
        }

        $tables = $this->config->item('tables','member');

        //validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('signup_user_validation_fname_label'), 'required|xss_clean');
        $this->form_validation->set_rules('last_name', $this->lang->line('signup_user_validation_lname_label'), 'required|xss_clean');
        $this->form_validation->set_rules('email', $this->lang->line('signup_user_validation_email_label'), 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', $this->lang->line('signup_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('signup_user_validation_password_confirm_label'), 'required');

        $this->form_validation->set_rules('company', $this->lang->line('signup_user_validation_company_label'), 'xss_clean');
        $this->form_validation->set_rules('phone', $this->lang->line('signup_user_validation_phone_label'), 'numeric|xss_clean');



        if ($this->form_validation->run() == true)
        {

            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email    = strtolower($this->input->post('email'));
            $input_password = $this->input->post('password');
            /*ovde se enkriptira pasvordo za vo temporarnata tabela*/
            $password = $this->encrypt->encode($input_password);

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
            );

            $temp_additional_data = array();
            foreach($additional_data as $key => $value){
                $temp_additional_data[] = $key . '|' . $value;
            }

            // Send E-mail
            //GENERATE A RANDOM KEY
            $random_key = md5(uniqid());

            $this->load->library('email', array('mailtype' => 'html'));

            $this->email->from('bozidar.ilio@gmail.com', 'Bobo');
            $this->email->to($this->input->post('email'));
                            
            $this->email->subject('Confirm your acount.');
            
            $email_message = '<p>Thank you ' . $this->input->post('email') . ' for signing up.</p>';
            $email_message .= '<p>Click <a href="' . base_url() . 'client/confirm_signup/';
            $email_message .= $random_key . '" >here</a> to confirm your account.</p>';
            
            $this->email->message($email_message); 


           if($this->email->send()){
                //Ako uspesno e ispraten mailot togas zacuvajgo userot vo temporarna tabela temp_users 
                $this->load->model('client/temp_users_m');
                $this->temp_users_m->save(array(
                    'user_name' => $username,
                    'email' => $email,
                    'password' => $password,
                    'additional_data' => implode(',', $temp_additional_data),
                    'email_key' => $random_key,
                    'date_email_created' => (now('Europe/Skopje'))
                ));
                $this->session->set_flashdata('message', 'Check your email to confirm new acount!');
                redirect('client/login', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'message failed to send');
            } 

        }
        else
        {
            //display the register user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->load->model('page/page_m');
            $this->data['subview_data'] = $this->page_m->get_by_slug('signup');

            // TO DO: ova treba da go smenam ko ce ja napravam tabelata so pages
            // $subview_data = new stdClass();
            // $subview_data->layout = 'authentication';
            // $this->data['subview_data'] = $subview_data;

            $this->_render_page('signup', $this->data);
        }

    }

	public function confirm_signup($email_key=''){

		if ($email_key == '') {
			$this->session->set_flashdata('message', 'Client email confirmation failed!');
	        redirect('client/signup', 'refresh');
			return FALSE;
		}

		$temp_user = $this->temp_users_m->get_by_email_key($email_key);

		if (!empty($temp_user)) {
			if ($temp_user->email_key == $email_key) {

				$username = $temp_user->user_name;
				$email 	  = $temp_user->email;
				/*ovde se dekodira pasvordot na userot od temorarnata tabela*/
				$password = $this->encrypt->decode($temp_user->password);
				$additional_data = $temp_user->additional_data;
				if($this->ion_auth->register($username, $password, $email, $additional_data)){

					//Ako e uspesna registracijata IZBRISI go temp userot
					$this->temp_users_m->delete($temp_user->temp_user_id);

					$this->session->set_flashdata('message', $this->ion_auth->messages());
	            	redirect('client/login', 'refresh');
	            	return;
        		} else {
        			$this->session->set_flashdata('message', 'Client email confirmation failed!');
	            	redirect('client/signup', 'refresh');
        		}
			}
		} else {
			$this->session->set_flashdata('message', 'Client email confirmation failed!');
	        redirect('client/signup', 'refresh');
		}


	}/*end fun confirm_signup*/


	
	//log the user in
	function login()
	{
		$this->data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{

				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());

				// If user is admin redirect to admin dashboard page, else go to frontend home page
				if ($this->ion_auth->is_admin()){
					redirect('admin', 'refresh');
				} else {
					redirect('/', 'refresh');
				}

				
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('client/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			$this->_render_auth_page('login', $this->data);
		}
	}

	//log the user out
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('/', 'refresh');
	}

	//change password
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('client/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('client/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password()
	{ 

		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_username_identity_label'), 'required');	
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');	
		}
		
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_auth_page('forgot_password', $this->data, $render=false);
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
			}

	            	if(empty($identity)) {
	            		
	            		if($this->config->item('identity', 'ion_auth') == 'username')
		            	{
                        	$this->ion_auth->set_message('forgot_password_username_not_found');
		            	}
		            	else
		            	{
		            		$this->ion_auth->set_message('forgot_password_email_not_found');
		            	}

                		$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("client/forgot_password", 'refresh');
                		return;
            		}

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("client/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());

				$this->_render_auth_page('forgot_password', $this->data, $render=false);
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->_render_auth_page('reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('client/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("client/forgot_password", 'refresh');
		}
	}

	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_auth_page($view, $data=null, $render=false) {

		/*LOGIN MORA DA SE POJAVUVA NA CIST LAYOUT so vcitani js css i bootstrap*/
		$data['module'] = get_class($this);
        $data['subview'] = $view;
        $data['contents'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, in, quod nostrum nihil repudiandae asperiores commodi! Iure, quia illo unde libero eligendi minus commodi ipsum eius nulla assumenda suscipit numquam!';
        $data['subview_data'] = $data;

        echo Modules::run('layout/_authentication', $data);

	}

	function _render_page($view, $data=null) {

        $data['module'] = get_class($this);
        $data['subview'] = $view;

        $layout = $data['subview_data']->layout;

        echo Modules::run('layout/_'.$layout, $data);

    }


    public function delete_unregistred() {

    	/*Izbrisigo clientot od temp tabelata ako ne se registrira vo rok od 3 dena*/
		$this->temp_users_m->delete_unregistred();
    		
    }

    public function contact_me(){

		// Check for empty fields
		if(empty($_POST['name'])  		||
		   empty($_POST['email']) 		||
		   empty($_POST['captcha']) 	||
		   empty($_POST['encrypt']) 	||
		   empty($_POST['message'])		||
		   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
		   {
			echo "false";
			return false;
		   }
			
		$name = $_POST['name'];
		$email_address = $_POST['email'];
		$captcha = $_POST['captcha'];
		$encrypt = $_POST['encrypt'];
		$message = $_POST['message'];


		/*ovde se dekodira captchata*/
		$decrypt = $this->encrypt->decode($encrypt);
		if ($decrypt != $captcha) {
			echo "false";/*ova mi treba, vo js spored nego ja proveruvam captcha dali e uspesna*/
			return false;
		}


/*
$this->load->library('email', array('mailtype' => 'html'));

$config['protocol'] = "smtp";
// $config['smtp_host'] = "ssl://smtp.gmail.com";
$config['smtp_host'] = "mail1.runhosting.com";
// $config['smtp_port'] = "465";
$config['smtp_port'] = "25";
$config['smtp_user'] = "bozidar.mob@gmail.com"; 
$config['smtp_pass'] = "kendimata";
$config['charset'] = "utf-8";
$config['mailtype'] = "html";
$config['newline'] = "\r\n";

$this->email->initialize($config);

$this->email->from($email_address, $name);
$list = array('bozidar.mob@gmail.com');
$this->email->to($list);
$this->email->reply_to('bozidar.ilio@gmail.com', 'Reply-To from suna.eu.pn');
$this->email->subject('This is an email test');
$this->email->message('It is working. Great!');
		if($this->email->send()){
			file_put_contents('test.txt', 'Message successfully sent!');
			$this->session->set_flashdata('message', 'Message successfully sent.');
			return true;
		} else {
			file_put_contents('test.txt', 'Message not sent!');
			echo "false";
			return false;
		}

*/


		
		$this->load->library('email', array('mailtype' => 'html'));

        $this->email->from($email_address, $name);
        $this->email->to('bozidar.mob@gmail.com');
                        
        $this->email->subject("Website Contact Form:  $name");

        $email_body = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nCaptcha: $captcha\n\nMessage:\n$message";
        $email_message = '<p>' . $email_body . '</p>';
        
        $this->email->message($email_message); 


		if($this->email->send()){
			file_put_contents('test.txt', 'Message successfully sent! on: ' . now(), FILE_APPEND | LOCK_EX);
			echo "true";
			return true;
		} else {
			file_put_contents('test.txt', 'Message not sent! on: ' . now(), FILE_APPEND | LOCK_EX);
			echo "false";
			return false;
		}


/*
		// Create the email and send the message
		$to = 'bozidar.mob@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
		$email_subject = "Website Contact Form:  $name";
		$email_body = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nCaptcha: $captcha\n\nMessage:\n$message";
		$headers = "From: noreply@yourdomain.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
		$headers .= "Reply-To: $email_address";	
		mail($to,$email_subject,$email_body,$headers);
		return true;
		*/

    }/*end fun contact_me*/

/*END CLASS*/}