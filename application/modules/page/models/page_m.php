<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Page_m extends MY_Model
{
    protected $_table_name = 'pages';
    protected $_order_by = 'position';
    // protected $_order_by = array('order', 'id');
    public $rules = array(
        'parent_id' => array(
            'field' => 'parent_id', 
            'label' => 'Parent', 
            'rules' => 'trim|intval'
        ),
        'name' => array(
            'field' => 'name', 
            'label' => 'Name', 
            'rules' => 'required|callback__alpha_space|max_length[100]|callback__unique_name|xss_clean'
        ), 
        'title' => array(
            'field' => 'title', 
            'label' => 'Title', 
            'rules' => 'required|xss_clean'
        ), 
        'layout' => array(
            'field' => 'layout', 
            'label' => 'Layout', 
            'rules' => 'trim|required|xss_clean'
        ) 
    );  

    public $edit_page_rules = array(
        'name' => array(
            'field' => 'name', 
            'label' => 'Name', 
            'rules' => 'required|callback__alpha_space|max_length[100]|callback__unique_name|xss_clean'
        ), 
        'title' => array(
            'field' => 'title', 
            'label' => 'Title', 
            'rules' => 'required|xss_clean'
        )
    );

    function __construct ()
    {
        parent::__construct();

        $sess_lang = $this->session->userdata('language');

        if ($sess_lang != 'english') {
            if (in_array($sess_lang, config_item('languages'))) {
                $language = substr($sess_lang, 0, 3);
                $this->_table_name = $this->_table_name . '_' . $language;
            }
        }
    }

    public function get_new ()
    {
        $page = new stdClass();
        $page->parent_id = 0;
        $page->slug = '';
        $page->name = '';
        $page->title = '';
        $page->contents = '';
        $page->visible = 1;
        $page->layout = 'basic';
        $page->template = '';
        return $page;
    }

    
    public function delete ($id)
    {
        // Delete a page
        parent::delete($id);
        
        // Reset parent ID for its children
        $this->db->set(array(
            'parent_id' => 0
        ))->where('parent_id', $id)->update($this->_table_name);
    }

    public function save_order ($pages){

        if (count($pages)) {
            foreach ($pages as $key => $list_str) {
                $id = intval(str_replace('list_', '', $list_str));
                $this->db->set(array('order'=>$key+1))->where('id', $id)->update($this->_table_name);
            }
        }

    }


    private function get_top_menu_slug(){

        $this->db->select('slug, `position`');
        $this->db->where('parent_id', 0);
        $this->db->order_by('position');
        $tops = $this->db->get($this->_table_name)->result_array();

        foreach ($tops as $top) {
            $key = $top['position'];
            $t[$key] = $top['slug'];
        }

        return (!empty($t))? $t : array();

    }

    /* Ovaa ja koristam vo get_public_menu()*/
    private function sort_array_of_array($array, $subfield) {

        $sortme = $array;

        $sortarray = array();
        foreach ($array as $key => $row)
        {
            $sortarray[$key] = $row[$subfield];
        }

        array_multisort($sortarray, SORT_ASC, $sortme);

        return $sortme;
    }

    public function get_public_menu (){

        $pages = $this->db->get($this->_table_name)->result_array();

        $array = array();
        foreach ($pages as $page) {
            if (! $page['parent_id']) {
                $array[$page['id']] = $page;
            }
            else {
                $array[$page['parent_id']]['children'][] = $page;
            }
        }

        // Sort top level pages by position
        $array = $this->sort_array_of_array($array, 'position');

        // Ovde e bitno dupetljata da e so REFERENCA!! (&$a)
        foreach ($array as &$a) {
            if (array_key_exists('children', $a)) {
                // Sort second level pages by position
                $a['children'] = $this->sort_array_of_array($a['children'], 'position');
            }
        }

        return $array;

    }/*end fun get_public_menu()*/


    public function get_nested ()
    {
        $pages = $this->db->get($this->_table_name)->result_array();
        
        $array = array();
        foreach ($pages as $page) {
            if (! $page['parent_id']) {
                $array[$page['id']] = $page;
            }
            else {
                $array[$page['parent_id']]['children'][] = $page;
            }
        }
        return $array;
    }

    public function get_with_parent ($id = NULL, $single = FALSE)
    {
        $this->db->select('pages.*, p.slug as parent_slug, p.title as parent_title');
        $this->db->join('pages as p', 'pages.parent_id=p.id', 'left');
        return parent::get($id, $single);
    }

    public function get_no_parents ()
    {
        // Fetch pages without parents
        $this->db->select('id, title');
        $this->db->where('parent_id', 0);
        $pages = parent::get();
        
        // Return key => value pair array
        $array = array(
            0 => 'No parent'
        );
        if (count($pages)) {
            foreach ($pages as $page) {
                $array[$page->id] = $page->title;
            }
        }
        
        return $array;
    }

    public function get_by_slug($slug){
        $where = array('slug' => $slug);
        return parent::get_by($where, $single = TRUE);
    }

    public function get_by_layout($layout){
        $where = array('layout' => $layout);
        return parent::get_by($where, $single = FALSE);
    }

    public function get_by_id($id){
        $where = array('id' => $id);
        return parent::get_by($where, $single = TRUE);
    }

    public function get_by_parent_id($parent_id){
        $where = array('parent_id' => $parent_id);
        return parent::get_by($where, $single = TRUE);
    }

    public function get_title($id){
        return ($id)? $this->get_by_id($id)->title : '';
    }   

    public function get_by_template($template){
        $where = array('template' => $template);
        return parent::get_by($where, $single = FALSE);
    }


    public function get_max_position($template){

        $this->db->select_max('position', 'max_position');/*position AS max_position*/
        $this->db->where('template', $template);
        $query = $this->db->get($this->_table_name);
        $row = $query->row();

        $max_position = intval($row->max_position);

        return ($max_position)? $max_position : 0;
    }

    public function get_array_of_pages_slug(){

        $pages = array();

        $menu = $this->get_public_menu();

        foreach ($menu as $m) {/*menu*/
            $pages[] = $m['slug'];
            if (isset($m['children'])) {
                if (is_array($m['children'])) {
                   foreach ($m['children'] as $s) {/*sub-menue*/
                       $pages[] = $s['slug'];
                   }
                }
            }
        }

        return $pages;

    }

    public function get_array_of_pages_slugs_by_template($template){

        $pages = array();

        $menu = $this->get_by_template($template);

        foreach ($menu as $m) {/*menu*/
            $pages[] = $m->slug;
            if (isset($m->children)) {
                if (is_array($m->children)) {
                   foreach ($m->children as $s) {/*sub-menue*/
                       $pages[] = $s['slug'];
                   }
                }
            }
        }

        return $pages;

    }


    public function admin_sidebar_pages ()
    {
        $pages = $this->db->get($this->_table_name)->result_array();
        
        $array = array();
        foreach ($pages as $page) {

            $tmp = array();
            $tmp = $page;
            $tmp['has_child'] = ($this->has_child($page['id'], $pages))? 1 : 0;
            
            if (! $page['parent_id']) {
                $array[$page['id']] = $tmp;
            }
            else {
                $array[$page['parent_id']]['children'][] = $tmp;
            }
        }

        return $array;
    }



    public function has_child($id, $pages){

        foreach ($pages as $page) {
            if ($page['parent_id'] == $id) {
                return true;
            }
        }

        return false;
    }


}