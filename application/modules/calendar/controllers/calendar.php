<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Calendar extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('dt');//My DateTime Library
        $this->data['dt'] = $this->dt;
    }

    public function index()
    {
        $data = array(
            'content' => 'calendar/index'
        );

        $data['dt'] = $this->data['dt'];
        $this->load->view('index', $data);
    }

    public function add_sub(){

        $this->dt->setDate(2015, 1, 31);
        $this->dt->set_format('l, F j, Y');

        eh1($this->dt);

        $interval = new DateInterval('P1M');
        $this->dt->add($interval);

        eh1($this->dt); 

        $this->dt->sub($interval);
        
        eh1($this->dt); 
    }

}