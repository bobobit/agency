<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Fontchooser extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('fontchooser_m');
    }

    public function index()
    {
        $data = array(
            'content' => $this->fontchooser_m->get(),
            'font_family' => $this->_font_family()
        );
        $this->load->view('index', $data);
    }

    public function _font_family(){

        $fpath = 'assets/sass/agency-sass-mac/_font_face.scss';
        $fcontents = file_get_contents($fpath);
        $pattern = "%font-family: \"(.*)\";%";

        preg_match_all($pattern, $fcontents, $matches);

        /*
            $matches[0]  full pattern (ovde go dava celiot patern po koj sto barav)
            $matches[1]  first captured parenthesized subpattern (ovde e tekstot od prvite zagradi) -!!ova mi treba
        */

        $font_family = $matches[1];

        return $font_family;

    }
}