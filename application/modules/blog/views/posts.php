<div class="row">

<?php if (sizeof($posts)) { ?>

<?php if (isset($posts[0]->category) && !empty($posts[0]->category)) { ?>
<div class="row col-lg-12">
	<h3><i>Posts in Category <?php echo $posts[0]->category->name; ?></i></h3>
</div>
<?php } ?>

<?php foreach ($posts as $pst) { ?>
	<div class="row col-lg-12">
		<h3 class="clearfix">
			<div class="col-lg-9 pull-left">
				<a href="<?php echo site_url('blog/post/' . $pst->id); ?>">
					<big>
						<?php echo $pst->title; ?>
					</big>
				</a>
			</div>
			<div class="col-lg-3 pull-right">
				<a href="<?php echo site_url('blog/post/' . $pst->id); ?>">
					<small>
						<span class="glyphicon glyphicon-time"></span> 
						<?php echo date("j F Y", strtotime($pst->created)); ?>
					</small>
				</a>
			</div>
		</h3>
	</div>
	<div class="col-lg-12">
	<hr>
	</div>
	<div class="col-lg-12">
	<?php if (strlen($pst->body) > 500) { ?>
		
			<?php echo substr($pst->body, 0, 500); ?>... 
			<a href="<?php echo site_url('blog/post/' . $pst->id); ?>"> read post</a>
		
	<?php } else { ?>
		<p><?php echo $pst->body; ?></p>
	<?php }/*end if strlen*/ ?>

	</div>
<?php }/*end foreach posts*/ ?>

	<div class="col-lg-12">
	<?php echo $pagination_links; ?>
	</div>

<?php }/*end if posts*/  else { ?>
	<div class="col-lg-12">
	<h3><i>There is no posts to be displayed!</i></h3>
	</div>
<?php } ?>
</div>