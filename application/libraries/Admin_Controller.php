<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{

    function __construct() {
        parent::__construct();

		// Login check
		$exception_uris = array(
				'user/login',
				'user/logout'
		);

		$this->load->library('ion_auth');

		if (isset($_GET['forgot_password'])) {
			return;
		}

		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->ion_auth->logged_in() == FALSE) {
				redirect('user/login');
			} elseif ($this->ion_auth->logged_in() == TRUE && $this->ion_auth->is_admin() == FALSE) {
				redirect('user/login');
			}
		}

    }/*end construct*/

/*END class Admin_Controller*/}