    <!-- Back to top link -->
    <span id="top-link-block" class="hidden">
        <a href="#top" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
            <i class="fa fa-angle-up"></i>
        </a>
    </span><!-- /top-link-block -->

	<!-- FOOTER -->
	<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; <?php echo lang('site_title'); ?> <?php echo date('Y'); ?></span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>




    <!-- jQuery -->
    <script src="<?php echo site_url('assets/js/jquery.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?php echo site_url('assets/js/classie.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url('assets/js/agency.js'); ?>"></script>

    <!-- Bootstrap-Image-Gallery -->
    <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <script>
        if (typeof window.blueimp == 'undefined') {
            jQuery.getScript("<?php echo site_url('assets/image_gallery/js/jquery.blueimp-gallery.min.js'); ?>");
            jQuery.getScript("<?php echo site_url('assets/image_gallery/js/blueimp-gallery.min.js'); ?>");
        };
    </script>

    <!-- My Custom Script -->
    <script src="<?php echo site_url('assets/js/scripts.js'); ?>"></script>

</body>

</html>