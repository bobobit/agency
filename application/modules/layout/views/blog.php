<?php $this->load->view('partials/blog_header', $this->data); ?>
<?php $this->load->view('partials/blog_navigation', $this->data); ?>
<div class="container">
	<section id="blog">
		<div class="row">
			<div class="col-lg-9">
				<?php $this->load->view($module . '/' . $subview, $subview_data); ?>
			</div>
			<div class="col-lg-3">
				<?php $this->load->view($module . '/' . $sidebar, $sidebar_data); ?>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('partials/blog_footer'); ?>