<?php
class MY_Pagination extends CI_Pagination {

	function __construct() {

		parent::__construct();
		
	}/*end construct()*/

	public function get_per_page(){
		return $this->per_page;
	}

	public function initialize($pagination_params){

		foreach ($pagination_params as $key => $value) {
			$this->$key = $value;
		}

		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		foreach ($config as $key => $value) {
			$this->$key = $value;
		}

	}

}