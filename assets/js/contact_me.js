$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour

            var base_url = $('body').attr('data-base-url');
            var url = base_url + 'client/contact_me';
            var language = $('form#contactForm').attr('data-language');
            var success_alert_message = $('form#contactForm').attr('data-success-alert');
            var warning_alert_message = $('form#contactForm').attr('data-warning-alert');

            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var captcha = $("input#captcha").val();
            var encrypt_captcha = $('input#encryptCaptcha').val();
            var message = $("textarea#message").val();
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    name: name,
                    captcha: captcha,
                    encrypt: encrypt_captcha,
                    email: email,
                    message: message
                },
                cache: false,
                success: function(return_data) {
                    
                    if (return_data == 'true') {
                       // Success message
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>" + success_alert_message + "</strong>");
                        $('#success > .alert-success')
                            .append('</div>');

                        //clear all fields
                        $('#contactForm').trigger("reset");
                    } else {
                         // Unsuccess message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-danger')
                            .append("<strong>" + warning_alert_message + "</strong>");
                        $('#success > .alert-danger')
                            .append('</div>');
                    }



                    
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");

                    if (language == 'macedonian') {
                        $('#success > .alert-danger').append("<strong>Izvini " + firstName + ", serverot za elektronska po{ta ne e dostapen. Te molam obidise podocna!");
                    } else {
                        $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    }
                    
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});


/*CONTACT - change input font*/
//ova e zaradi fontot koga e vcitana makedonskata verzija na agency-sass-mac.css
$('section#contact input#email').click(function(){
    $(this).css('font-family', "sans-serif");
});
$('section#contact input#name').click(function(){
    $(this).css('font-family', "sans-serif");
});
$('section#contact input#captcha').click(function(){
    $(this).css('font-family', "sans-serif");
});
$('section#contact textarea#message').click(function(){
    $(this).css('font-family', "sans-serif");
});