<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding: 33px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">


<div class="panel panel-default">
    <div class="panel-heading">
        <h1><?php echo lang('index_heading');?></h1>
		<p><?php echo lang('index_subheading');?></p>
		<?php if($message != false){ ?>
        <div id="infoMessage" class="alert alert-warning">
        <?php echo $message;?>
        </div>
        <?php } ?>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    	<tr>
							<th><?php echo lang('index_fname_th');?></th>
							<th><?php echo lang('index_lname_th');?></th>
							<th><?php echo lang('index_email_th');?></th>
							<th><?php echo lang('index_groups_th');?></th>
							<th><?php echo lang('index_status_th');?></th>
							<th><?php echo lang('index_action_th');?></th>
						</tr>
                </thead>
                <tbody>
				<?php foreach ($users as $user):?>
					<tr>
			            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("user/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("user/deactivate/".$user->id, lang('index_active_link')) : anchor("user/activate/". $user->id, lang('index_inactive_link'));?></td>
						<td><?php echo anchor("user/edit_user/".$user->id, 'Edit') ;?></td>
					</tr>
				<?php endforeach;?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>

<p>
<?php echo anchor('user/create_user', lang('index_create_user_link'), array('class' => 'btn btn-primary'))?> 
<?php echo anchor('user/create_group', lang('index_create_group_link'), array('class' => 'btn btn-primary'))?>
</p>


            </div>
        </div>
    </div>
</div>