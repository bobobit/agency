<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Training extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_m');
    }

    public function index()
    {
        $this->session->set_flashdata('scroll_to', 'training');
        redirect(site_url());
    }

    public function trainings(){
        $data = array();
        $data['trainings'] = $this->training_m->get();
        $this->load->view('trainings', $data);
    }

    public function training_grid_section(){
        $data = array();
        $data['trainings'] = $this->training_m->get();
        $this->load->view('training_grid_section', $data);
    }

}