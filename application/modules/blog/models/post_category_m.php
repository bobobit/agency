<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Post_Category_m extends MY_Model
{

    protected $_table_name = 'post_category';
    protected $_order_by = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    
    public function get_by_id($id){
        $where = array('id' => $id);
        return parent::get_by($where, $single = TRUE);
    }
    
    public function get_by_category_id($category_id){
        $where = array('category_id' => $category_id);
        return parent::get_by($where, $single = FALSE);
    } 

    public function get_by_post_id($post_id){
        $where = array('post_id' => $post_id);
        return parent::get_by($where, $single = FALSE);
    }


}