<!-- Navigation -->
<style type="text/css"> 
  #admin-sidebar {
    margin-right: 27px;
  }
</style>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- /.navbar-header -->


<?php 
/*TO DO: 
Ova e privremeno duri ne ja napram tabelata so pages
ce napram edna niza pages manuelno
*/

// $pages = array(
//     array(
//         'id' => 1,
//         'name' => 'Home',
//         'has_child' => FALSE,
//         ),
//     array(
//         'id' => 2,
//         'name' => 'Portfolio',
//         'has_child' => FALSE,
//         ),    
//     array(
//         'id' => 3,
//         'name' => 'About',
//         'has_child' => FALSE,
//         )
// );

?>


    <ul class="nav navbar-top-links navbar-left">
        <li class="dropdown">

            <div class="dropdown">
                <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default" data-target="#" href="">
                    Menu <span class="caret"></span>
                </a>
                <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    
                    <li>
                        <a href="<?php echo site_url('admin/index'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('user/users'); ?>"><i class="fa fa-user fa-fw"></i> Users</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('admin/pages'); ?>"><i class="fa fa-file-text fa-fw"></i> Pages</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-submenu">
                        <?php /*ADMIN Photos*/ ?>
                        <a tabindex="-1" href="#"><i class="fa fa-sitemap fa-fw"></i> Photos </a>
                        <ul class="dropdown-menu">
                        <?php foreach ($pages as $key => $page) { ?>
                            <?php $class = ($page['id'] == $this->uri->segment(3))? 'class="active"' : ''; ?>
                            <li>

                                <a tabindex="-1" 
                                <?php if($page['has_child']) { ?>
                                     <?php echo $class; ?> href="#"><?php echo $page['name']; ?> <span class="fa arrow"></span></a>
                                <?php } else { ?>
                                     <?php echo $class; ?> href="<?php echo site_url('admin/upload_photo/' . $page['id']); ?>"><?php echo $page['name']; ?></a>
                                <?php } /*end if has_child*/ ?>

                                <?php /*SUBS*/ ?>
                                <?php if (isset($page['children'])) { ?>
                                    <?php if (is_array($page['children'])) { ?>
                                        <ul class="nav nav-third-level">
                                            <?php foreach ($page['children'] as $s) { ?>
                                                <li>
                                                    <a <?php echo $class; ?> href="<?php echo site_url('admin/upload_photo/' . $s['id']); ?>"><?php echo $s['name']; ?></a>
                                                </li>
                                            <?php } /*end foreach children*/ ?>
                                        </ul>
                                        <!-- /.nav-third-level -->
                                    <?php } ?>
                                <?php } /*end if isset children*/ ?>
                            </li>

                        <?php }/*end foreach pages*/ ?>

                        </ul> <!-- /.nav-second-level - Photos -->
                        
                    </li><!-- END ADMIN Photos -->

                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('admin/posts'); ?>"><i class="fa fa-file-text fa-fw"></i> Posts</a>
                    </li>

                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('admin/categories'); ?>"><i class="fa fa-file-text fa-fw"></i> Categories</a>
                    </li>

                    <li class="divider"></li>
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Training</a>
                        <ul class="dropdown-menu">
                          <li><a tabindex="-1" href="<?php echo site_url('admin/training'); ?>">eng</a></li>
                          <li><a href="<?php echo site_url('admin/training/mac'); ?>">mac</a></li>
                        </ul>
                    </li>

                    <li class="divider"></li>




                  <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">Hover me for more options</a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="#">Second level</a></li>
                      <li class="dropdown-submenu">
                        <a href="#">Even More..</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">3rd level</a></li>
                            <li><a href="#">3rd level</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Second level</a></li>
                      <li><a href="#">Second level</a></li>
                    </ul>
                  </li>
                </ul>
            </div>
        </li>
    </ul>


    <a class="navbar-brand pull-right" href="<?php echo site_url('/'); ?>" target="_blank">Frontend</a>


    <ul class="nav navbar-top-links navbar-right">

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links right-->

</nav>

