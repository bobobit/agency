    <!-- Healty Yoga Section -->
    <section id="healtyyoga" class="healtyyoga">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">JOGA VE@BI</h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('services_healtyyoga_gate_para'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    
                    <div style="margin:0 auto; width: 30%; padding-bottom: 35px;">
                        <img src="<?php echo site_url('assets/img/yoga_photo_za_bizins_sandrata_3.jpg'); ?>" class="img-responsive" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

                    <p>
                        Dokolku ste motivirani da rabotite na poleto na sopstveniot mentalen i fizi~ki napredok i da se oslobodite od sekojdnevniot stres, po~nete da praktikuvate redovno ve`bawe.
                    </p>
                    <p>
                        Programata e prilagodena za rabota vo grupa i toa kako za individualci taka i za raboteni timovi. Ve`bite se nameneti za sovremeniot, dinami~en i poln so stres zapaden ~ovek.
                    </p>
                    <p>
                        Zdravjeto e na{e najgolemo bogatstvo i vo so~uvuvawe na na{eto zdravje redovno ve`bawe na joga ima neprocenliva va`nost. So pomo{ na joga ve`bite nie ja istegame, zajaknuvame i oblikuvame muskulnata i skeletna gradba a na toj na~in go koregirame dr`eweto (stavot) na teloto, go podobruvame zdravjeto i gi pottiknuvme site telesni funkcii.
                    </p>
                    <p>
                        Celta na joga ve`bite e da se vrati ramnote`ata vo teloto i umot.
                    </p>
                    <p>
                        Kako rezultat na redovno ve`bawe na jogа: 
                    </p>
                    <p>
                        <ul>
                            <li>se namaluva nivoto na akumuliran stres vo teloto i umot</li>
                            <li>se zgolemuva motivacijata i entuzijazmot na li~en i profesionalen plan,</li>
                            <li>se pottiknuva samodoverbata</li>
                            <li>se podobruva zdravjeto i </li>
                            <li>zgolemuva li~nata produktivnost i sposobnost</li>
                        </ul>
                    </p>
                    <p>
                        Eden ~as joga ve`bi vo sebe vklu~uva ve`bi za opu{tawe na muskulite, ve`bi za po~etno zagrevawe na teloto, ve`bi (asani) za celoto telo, ve`bi za di{ewe (pranajama) i kreativna vizuelizacija.
                    </p>
                    <p>
                        Ve`baweto na Joga se izveduva so kvalifikuvan joga instruktor, trae 1 ~as i 15 minuti 
                    </p>
                    <p>
                        <b>Redovnite ~asovi</b> se odr`uvaat sekoj ponedelnik i sreda od 19.45 ~as do 21 ~as vo detska gradinka Kor~agin (klon Pepela{ka) vedna{ do Crven Krst , ul. Leningradska br 102, Skopje  
                    </p>
                    <p>
                        ^asovite nameneti za rabotni organizacii se organiziraat soglasno potrebite i barawata na organizacijata i u~esnicite. 
                    </p>
                    <h3>Ekskluzivno !</h3>
                    <h4>Specijalna ponuda za rabotni organizacii </h4>
                    <p>
                        Oganizirame intenzivni vikend seminari  preku koja u~esnicite }e stanat sposobni da gi prepoznaat i upotrebat tehnikite i ve`bite so koi se unapreduva i podobruva sopstvenoto psiho-fizi~ko zdravje, a se nameneti za podobruvawe na entuzijazmot, motivacijata, timskata dinamika i zgolemuvawe na efikasnost na rabota. Obukata e interaktivna, teorija i ve`bi, a vo sebe vklu~uva primena na dinami~ni i joga/fizi~ki ve`bi i u~ewe na tehnika na di{ewe (Sudar{an Krija) koja pomaga vo brzo eliminirawe na stresot natalo`en vo na{eto telo. Na krajot na obukata u~esnicite dobivaat individualna programa za rabota so svoj sistem na ve`bi. Programata na ovoj seminar e dobiena od me|unarodnata fondacija Umetnost na @iveewe i ja vodat serticifirani u~iteli od istata fondacija.
                    </p>
                    <p>
                        Prijavete u~estvo vo grupite na na{iot <a href="<?php echo site_url('contact'); ?>"><b><i>Kontakt</i></b></a> .
                    </p>
                    
                </div>
            </div>

                  
            
        </div>
    </section>

    <section id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Galerija</h2>
                    <h3 class="section-subheading text-muted">Galerija na sliki za joga ve@bi</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

<!-- <?php /* TO DO: Ova e privremeno, ovie linkovi ce gi imam vo niza  i ce mu gi isprakjam na gallery modulot */ ?>-->
<!-- Image Gallery Links-->
<div id="links">
    <a href="<?php echo site_url('assets/img/IMG_0001.jpg'); ?>" title="photo1" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_0001.jpg'); ?>" alt="photo1">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_8897.jpg'); ?>" title="photo2" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_8897.jpg'); ?>" alt="photo2">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_8919.jpg'); ?>" title="photo3" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_8919.jpg'); ?>" alt="photo3">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_8936-1.jpg'); ?>" title="photo4" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_8936-1.jpg'); ?>" alt="photo4">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_8983.jpg'); ?>" title="photo5" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_8983.jpg'); ?>" alt="photo5">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9011.jpg'); ?>" title="photo6" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9011.jpg'); ?>" alt="photo6">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9071.jpg'); ?>" title="photo7" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9071.jpg'); ?>" alt="photo7">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9078.jpg'); ?>" title="photo8" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9078.jpg'); ?>" alt="photo8">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9100.jpg'); ?>" title="photo9" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9100.jpg'); ?>" alt="photo9">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9145.jpg'); ?>" title="photo10" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9145.jpg'); ?>" alt="photo10">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9152.jpg'); ?>" title="photo11" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9152.jpg'); ?>" alt="photo11">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9209.jpg'); ?>" title="photo12" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9209.jpg'); ?>" alt="photo12">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9211.jpg'); ?>" title="photo13" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9211.jpg'); ?>" alt="photo13">
    </a>
    <a href="<?php echo site_url('assets/img/IMG_9255.jpg'); ?>" title="photo14" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/IMG_9255.jpg'); ?>" alt="photo14">
    </a>
</div>

                    <?php echo Modules::run('gallery'); ?>

                </div>
            </div>
        </div>
    </section>