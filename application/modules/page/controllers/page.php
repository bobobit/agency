<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Page extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->data['redirect_pages'] = array('index', 'blog', 'login', 'healtyyoga');
        $this->data['one_page_pages'] = array('services', 'portfolio', 'about', 'contact');

        $sess_scroll_to = $this->session->flashdata('scroll_to');
        $this->data['scroll_to'] = ($sess_scroll_to != '')? $sess_scroll_to : '';

        // Redirektiraj na odredenata strana pritoa da se izbegni uri segmentot 'page' 
        $this->_page_redirect();

    }

    public function index()
    {/*!Index mora da e prazen oto se vcituva po default, namesto ova se povikuva privatnata _index()*/}


    private function _page_redirect(){

        $slug = ($this->uri->segment(1))? $this->uri->segment(1) : 'index';

        if (in_array($slug, $this->data['one_page_pages'])) {
            $this->data['scroll_to'] = $slug;
            $this->session->set_flashdata('scroll_to', 'portfolio');
            $this->_index();
        } elseif (in_array($slug, $this->data['redirect_pages'])) {
            $method = '_'.$slug;
            $this->$method();
        } else {
            $this->_error_404();
        }

    }/*end fun _page_redirect()*/


    private function _index()
    {
        $data = array(
            'content' => 'page/index'
        );

        $data['subview_data'] = new stdClass();

        $data['subview_data']->layout = 'one_page';
        $data['scroll_to'] = $this->data['scroll_to'];

        $this->_render_page('index', $data);
    }

    private function _error_404()
    {

        $message = '';
        $message .= '<p>';
        $message .= 'Page not found!';
        $message .= '</p>';
        $message .= '<p>';
        $message .= 'Go back to site ';
        $message .= '<a href="' . site_url() . '">';
        $message .= site_url();
        $message .= '</a>';
        $message .= '</p>';

        $data = array(
            'content' => 'page/error_404',
            'heading' => 'Error 404',
            'message' => $message
        );
        $this->load->view('errors/error_404', $data);
    }

    private function _login(){
        redirect(site_url('client/login'));
    }
/*
    private function _healtyyoga(){
        
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'image_gallery';

        $this->_render_page('healtyyoga', $data);

    }*/

    function _render_page($view, $data=null) {

        $data['module'] = strtolower(get_class($this));
        $data['subview'] = $view;

        $layout = $data['subview_data']->layout;

        echo Modules::run('layout/_'.$layout, $data);

    }

}