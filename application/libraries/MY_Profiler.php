<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Profiler extends CI_Profiler{

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Profiler run()
	 *
	 * @Override
	 */
	public function run(){

		$output = parent::run();

		// Vo config/config.php se definira profiler_log (TRUE | FALSE)
		$log = $this->CI->config->config['profiler_log'];

		if ($log) {
			$this->_profiler_log($output);
			return '';
		} else {
			return $output;
		}

	}

	/**
	 * Profiler log
	 */
	public function _profiler_log($output)
	{
		/*namesto profilerot da vrakja output t.e da se pecati vo view, 
		sodrzinata od profilerot se zapisuva vo log fajl*/
		file_put_contents(APPPATH . 'logs/profiler.php', $output);
	}

}