    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-shrink navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo site_url(); ?>"><?php echo lang('site_title'); ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#top"></a>
                    </li>

                    <li class="active">
                        <a class="page-scroll" href="#softskillstraining"><?php echo lang('softskills_title'); ?></a>
                    </li>

                    <li>
                        <a class="page-scroll" href="#gallery">GALERIJA</a>
                    </li>

                    <li>
                        <?php echo Modules::run('switchlang/swlang_anchor'); ?>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>