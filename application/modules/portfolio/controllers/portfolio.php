<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Portfolio extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('portfolio_m');
    }

    public function index()
    {
        $this->session->set_flashdata('scroll_to', 'portfolio');
        redirect(site_url());
    }

    public function projects(){
        $data = array();
        $data['projects'] = $this->portfolio_m->get();
        $this->load->view('projects', $data);
    }

    public function portfolio_grid_section(){
        $data = array();
        $data['projects'] = $this->portfolio_m->get();
        $this->load->view('portfolio_grid_section', $data);
    }

}