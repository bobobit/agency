<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Category_m extends MY_Model
{

    protected $_table_name = 'categories';
    protected $_order_by = 'priority';

    public function __construct()
    {
        parent::__construct();
    }

    
    public function get_by_id($id){
        $where = array('id' => $id);
        return parent::get_by($where, $single = TRUE);
    }
    
    public function get_by_parent_id($parent_id){
        $where = array('parent_id' => $parent_id);
        return parent::get_by($where, $single = FALSE);
    }


    public function get_by_post_id($post_id){

/*
SELECT * 
FROM post_category 
JOIN categories 
ON categories.id=post_category.category_id 
WHERE post_category.post_id=25 ;

		eh1($this->db->last_query());
*/

		$this->db->select('*');
		$this->db->from('post_category');
		$this->db->join('categories', 'categories.id=post_category.category_id');
		$this->db->where('post_category.post_id', $post_id); 

        return $this->db->get()->result();
    }


}