<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['blog_search_title']				= 'Post Search';
$lang['blog_post_categories_title']		= 'Post Category';
$lang['blog_recent_posts_title']		= 'Recent Posts';
$lang['blog_posts_by_title_title']		= 'Posts by Title';
$lang['blog_posts_by_date_title']		= 'Posts by Date';
$lang['blog_recent_comments_title']		= 'Recent Comments';
$lang['blog_post_search_no_result']		= 'No results were found';
