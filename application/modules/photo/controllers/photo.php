<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Photo extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $this->load->view('index', $data);
    }

    public function preview($id){

    	$this->load->model("photo/photo_m");
        $photo = $this->photo_m->get_by_id($id);

        $data = array();
        $data['layout'] = 'bigpicture';
        $data['photo'] = $photo;

        $data['return_url'] = $this->input->post('return_url');

        $data['subview_data'] = array(
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                        Animi, asperiores, eaque, maxime saepe voluptas veniam 
                        architecto dolorum error quod perferendis dignissimos 
                        eligendi ipsa quibusdam magni facere eveniet amet. Commodi, minima.'
                        );

    	$this->_render_page('preview', $data);
    }



    function _render_page($view, $data=null) {

        $data['module'] = get_class($this);
        $data['subview'] = $view;

        $layout = $data['layout'];

        echo Modules::run('layout/_'.$layout, $data);

    }

}