<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Training_m extends MY_Model
{

    protected $_table_name = 'training';
    protected $_order_by = 'position';

    public function __construct()
    {
        parent::__construct();

        $sess_lang = $this->session->userdata('language');

        if ($sess_lang != 'english') {
            if (in_array($sess_lang, config_item('languages'))) {
                $language = substr($sess_lang, 0, 3);
                $this->_table_name = $this->_table_name . '_' . $language;
            }
        }
    }
}