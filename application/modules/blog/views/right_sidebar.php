<script language="javascript" type="text/javascript">
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>
<div class="row">
	<div class="col-lg-12">
		<div class="well">
            <h4><?php echo lang('blog_search_title'); ?></h4>
            <div class="input-group">
                <input type="text" class="form-control" name="blog_search_query" id="blog_search_query">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="blog_search_button">
                        <i class="fa fa-search fa-fw"></i>
                    </button>
                </span>
            </div>
            <!-- /.input-group -->
            <!-- Ova e rezerviran div za BLOG SEARCH sodrzinata pratena od AJAX-->
            <div id="blog_search" data-url="<?php echo site_url('blog/blog_search_ajax'); ?>">
                <!-- AJAX DATA HERE -->
            </div>
        </div>
	</div>
    <?php if (count($categories)) { ?>
    <div class="col-lg-12">
        <div class="well">
            <h4><?php echo lang('blog_post_categories_title'); ?></h4>
            <ul class="list-unstyled list-inline">
                <?php foreach ($categories as $category) { ?>
                <li><a href="<?php echo site_url('blog/postsbycategory/' . $category->id); ?>">
                    <small><span class="badge"><?php echo $category->name; ?></span></small>
                </a></li>
                <?php }/*end foreach categories*/ ?>
            </ul>
        </div>
    </div>
    <?php }/*end if categories*/ ?>
    <div class="col-lg-12">
        <div class="well">
            <h4><?php echo lang('blog_recent_posts_title'); ?></h4>
            <ul class="list-unstyled">
                <?php foreach ($recent_posts as $pst) { ?>
                <li><a href="<?php echo site_url('blog/post/' . $pst->id); ?>">
                    <?php echo $pst->title; ?>
                </a></li>
                <?php }/*end foreach recent_posts*/ ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="well">
            <h4><?php echo lang('blog_posts_by_title_title'); ?></h4>
            <div class="input-group">
                <input type="text" class="form-control" name="post_search_query" id="post_search_query">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="post_search_button">
                        <i class="fa fa-search fa-fw"></i>
                    </button>
                </span>
            </div>
            <!-- /.input-group -->
            <!-- Ova e rezerviran div za BLOG SEARCH sodrzinata pratena od AJAX-->
            <div id="post_search" data-url="<?php echo site_url('blog/post_search_ajax'); ?>">
                <!-- AJAX DATA HERE -->
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="well">
            <h4><?php echo lang('blog_posts_by_date_title'); ?></h4>
            <iframe id="iframeDatepicker"  name="iframeDatepicker"
              width="100%"
              height="90px"
              src="<?php echo site_url('datepicker/default_funcionality'); ?>"
              frameborder="0"
              scrolling="no" 
              onload='javascript:resizeIframe(this);'
              >
            </iframe>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="well">
            <h4><?php echo lang('blog_recent_comments_title'); ?></h4>
            <ul class="list-unstyled">
                <?php foreach ($recent_comments as $cmnt) { ?>
                <li><a href="<?php echo site_url('blog/post/' . $cmnt->post_id); ?>">
                    <?php echo date("F j, Y", strtotime($cmnt->created)); ?>
                    <small><span class="glyphicon glyphicon-user"></span></small>
                    <?php echo $cmnt->commentator; ?>
                </a></li>
                <?php }/*end foreach recent_comments*/ ?>
            </ul>
        </div>
    </div>
</div>
 