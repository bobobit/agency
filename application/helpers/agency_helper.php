<?php

function get_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';

	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav navbar-nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;

		foreach ($array as $item) {
				
			if ($item['visible'] == 0) { continue; }/*ako e setirana na not visible preskoknija*/	

			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">' : '<li class="dropdown">';
				$str .= '<a  class="dropdown-toggle" data-toggle="dropdown" href="' . site_url(e($item['slug'])) . '">' . e($item['name']);
				$str .= '<b class="caret"></b></a>' . PHP_EOL;
				$str .= get_menu($item['children'], TRUE);
			}
			else {
				$str .= $active ? '<li class="active">' : '<li>';
				$str .= '<a href="' . site_url($item['slug']) . '">' . e($item['name']) . '</a>';
			}
			$str .= '</li>' . PHP_EOL;

		}

		$str .= '</ul>' . PHP_EOL;
	}

	return $str;
}

function get_one_page_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';

	if (count($array)) {

		foreach ($array as $item) {
				
			if ($item['visible'] == 0) { continue; }/*ako e setirana na not visible preskoknija*/	

			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			$str .= $active ? '<li class="active">' : '<li>';
			$str .= ' <a ';
			if ($item['layout'] == 'one_page') {
				$str .= ' class="page-scroll" ';
				$str .= ' href="';
				$str .= '#' . $item['slug'];
			} else {
				$str .= ' class="" ';
				$str .= ' href="';
				$str .= '' . site_url($item['slug']);
			}
			$str .= '">' . e($item['name']);
			// $str .= ' #' . site_url($item['slug']);
			
			$str .= ' </a> ';
			$str .= '</li>' . PHP_EOL;

		}

	}

	return $str;
}

function get_public_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';

	if (count($array)) {

		foreach ($array as $item) {
				
			if ($item['visible'] == 0) { continue; }/*ako e setirana na not visible preskoknija*/	

			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			$str .= $active ? '<li class="active">' : '<li>';
			$str .= ' <a ';
			$str .= ' class="" ';
			$str .= ' href="';
			$str .= '' . site_url($item['slug']);
			$str .= '">' . e($item['name']);
			$str .= ' </a> ';
			$str .= '</li>' . PHP_EOL;

		}

	}

	return $str;
}



function get_admin_menu ($array, $child = FALSE){

	$CI =& get_instance();
	$str = '';

	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav navbar-nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;

// ppr($array, 1);

		foreach ($array as $key => $item) {
				
			$active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
						
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">' : '<li class="dropdown">';
				$str .= '<a  class="dropdown-toggle" data-toggle="dropdown" href="' . site_url(e('admin/'.$item['slug'])) . '">';
				$str .= $CI->config->config['department_icons'][$item['slug']]  . PHP_EOL;
				$str .= e($item['name']);
				$str .= '<b class="caret"></b></a>' . PHP_EOL;
				$str .= get_admin_menu($item['children'], TRUE);
			}
			else {
				$str .= $active ? '<li class="active">' : '<li>';
				$str .= '<a href="' . site_url('admin/'.$item['slug']) . '">' . e($item['name']) . '</a>';
			}
			$str .= '</li>' . PHP_EOL;
			
			if ($child == TRUE) {
				$last_key = end(array_keys($array));
				if ($key == $last_key) {
					/*Edit Menue*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/edit/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-edit"></span> ' . PHP_EOL;
					$str .= 'Edit Menu ' . ucfirst($array[0]['template']) . ' </a></li>' . PHP_EOL;

					/*Reorder Menue*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/order/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-sort"></span> ' . PHP_EOL;
					$str .= 'Reorder Menu ' . ' </a></li>' . PHP_EOL;

					/*New Category*/
					$str .= '<li class="divider"></li>' . PHP_EOL;
					$str .= '<li><a href="' . site_url('admin/page/add/' . $array[0]['template']) . '">' . PHP_EOL;
					$str .= ' <span class="glyphicon glyphicon-plus"></span> ' . PHP_EOL;
					$str .= 'New Category</a></li>' . PHP_EOL;
				}
				
			}
		}/*end foreach*/

		$str .= '</ul>' . PHP_EOL;
	}

	return $str;
}

if(!function_exists('add_base_url_to_img_path')){
	function add_base_url_to_img_path($text, $images_path){

		$html = $text;

		// preg_match_all("%(<img alt=\".*?\" src=\")(.*?)(\" />)%", $html, $matches, PREG_SET_ORDER);

		$parts = preg_split("%(<img alt=\".*?\" src=\")%", $html, -1, PREG_SPLIT_DELIM_CAPTURE);

		$output = '';
		foreach ($parts as $p) {

			if (preg_match("%(<img alt=\".*?\" src=\")%", $p)) {
				$output .= $p . $images_path . '/';
			} else {
				$output .= $p;
			}
		}

		return $output;
	}
}
