<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Admin extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('page/page_m');
        $this->data['pages'] = $this->page_m->admin_sidebar_pages();

        $this->load->library('Grocery_CRUD');
        $this->load->library('image_crud');
    }

    public function index() {

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];

        $this->_render_page('index', $data);

    }

    public function dashboard() {

        $this->index();

    }


  

    public function iframe_photos() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('photo');
            $crud->set_table('photos');
            $crud->unset_add();
            $crud->change_field_type('name','readonly');
            $crud->change_field_type('url','readonly');
            $crud->change_field_type('thumb','readonly');

            $crud->callback_before_delete(array($this,'unlink_photo'));
                       
            $crud->where('category_id',$this->uri->segment(3));

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function unlink_photo($primary_key) {

        $photo = $this->db->where('id',$primary_key)->get('photos')->row();

        $url='assets/img/'  . $photo->url;
        $thumb_url='assets/img/thumbnails/'  . $photo->thumb;
         
        unlink($url);
        unlink($thumb_url);

        return true;
    }

    public function upload_photo() {

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];
        $data['subview_data']->page = $this->page_m->get_by_id($this->uri->segment(3));

        $this->_render_page('upload_photo', $data);
    }   

    public function iframe_upload_photo() {
        try{
            
            $image_crud = new image_CRUD();
    
            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('url');
            $image_crud->set_title_field('title');
            $image_crud->set_table('photos')
            ->set_relation_field('category_id')
            ->set_ordering_field('position')
            ->set_image_path('assets/img');


            $output = $image_crud->render();

        
            $this->load->view('subview.php',$output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function pages() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];

        $this->_render_page('pages', $data);
    }   

    public function iframe_pages() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('page');
            $crud->set_table('pages');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function pages_lang() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages_lang = $this->data['pages'];

        $this->_render_page('pages_lang', $data);
    }   

    public function iframe_pages_lang() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('page language');
            $crud->set_table('pages_lang');
            $crud->display_as('page_id','Page');

            $crud->callback_column($this->_unique_field_name('page_id'));

            $crud->set_relation('page_id','pages','name');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }


    public function posts() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];
/*        $this->load->model('blog/post_m');
        $this->data['posts'] = $this->post_m->get();
        $data['subview_data']->posts = $this->data['posts'];*/

        $this->_render_page('posts', $data);
    }   

    public function iframe_posts() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('post');
            $crud->set_table('posts');
            $crud->order_by('created','desc');
            
            $crud->set_relation_n_n(
                'categories',       //1. Ime na novata kolona
                'post_category',    //2. Ime na relacionata tabela 
                'categories',       //3. Ime na drugata tabela 
                'post_id',          //4. Primaren kluc-vrska od ovaa tabela 
                'category_id',      //5. Primaren kluc-vrska do drugata tabela 
                'name'              //6. Ime na edna kolona od drugata tabela 
                );

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function post_comments() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];

        $this->_render_page('post_comments', $data);
    }   

    public function iframe_post_comments() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Post Comment');
            $crud->set_table('post_comments');
            $crud->display_as('post_id','Post');
            $crud->order_by('created','desc');

            $crud->callback_column($this->_unique_field_name('post_id'),
                        array($this,'_column_short_post_title'));

            $crud->set_relation('post_id','posts','title');


            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    /*Using set_relation with callback_column problem (GroceryCRUD bug solved)*/
    private function _unique_field_name($field_name) {
        //This s is because is better for a string to begin with a letter and not with a number
        return 's'.substr(md5($field_name),0,8); 
    }
    public function _column_short_post_title($value,$row) {
        if (strlen($value)>30) {
            $short_title = substr($value, 0, 15) . ' [...] ' . substr($value, -15);
        } else {
            $short_title = $value;
        }
        return '<span title="' . $value . '" >'.$short_title. '</span>';
    }

    public function categories() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];

        $this->_render_page('categories', $data);
    }   

    public function iframe_categories() {
        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Category');
            $crud->set_table('categories');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }


    public function training($lang='eng') {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];


        $data['lang'] = $lang;

        $this->_render_page('training', $data);
    }  

    public function iframe_training($lang='eng') {
        try{
            
            $l = ($lang == 'eng')? '' : '_' . $lang;

            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Training' . ' ' . strtoupper($lang));
            $crud->set_table('training' . $l);

            $output = $crud->render();
            $output->lang = $lang;

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }


    public function profiler_logs(){

        $data['contents'] = file_get_contents(APPPATH . 'logs/profiler.php');

        $this->load->view('admin/profiler_logs', $data);
    }


    /*RENDER ADMIN PAGE*/
    function _render_page($view, $data=null) {

        $data['module'] = get_class($this);
        $data['subview'] = $view;

        $layout = $data['subview_data']->layout;

        echo Modules::run('layout/_'.$layout, $data);

    }

}