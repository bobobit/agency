    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><?php echo lang('site_title'); ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <?php /*TO DO: ova e privremeno duri da se odlucime koj fontovi ce bidat*/ ?>
                    <!--
                    <li>
                        <a href="<?php //echo site_url('fontchooser'); ?>">Font</a>
                    </li>
                    -->

                    <!-- Public Menu (li)-->
                    <?php echo get_one_page_menu($menu); ?>

                    <li>
                        <?php echo Modules::run('switchlang/swlang_ajax_anchor'); ?>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        <!-- /.container-fluid -->
    </nav>

    <div>
        
    </div>



