    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

<?php foreach ($projects as $project) { ?>

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal<?php echo $project->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo $project->name; ?></h2>
                            <p class="item-intro text-muted"><?php echo $project->subtitle; ?></p>
                            <img class="img-responsive" src="<?php echo site_url('assets/img/' . $project->img_url); ?>" alt="">
                            <p>
                                <?php echo $project->contents; ?>
                            </p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php }/*end for each projects*/ ?>