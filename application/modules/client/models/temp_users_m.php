<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Temp_users_m extends MY_Model {
	
	protected $_table_name = 'temp_users';
	protected $_primary_key = 'temp_user_id';
	protected $_order_by = 'temp_user_id';
	

	function __construct ()
	{
		parent::__construct();
	}

	
	public function get_new(){
		$user = new stdClass();
		$user->user_name = '';
		$user->email = '';
		$user->password = '';
		$user->additional_data = '';
		$user->email_key = '';
		$user->date_email_created = '';
		return $user;
	}


	public function get_by_temp_user_id($temp_user_id){
		$where = array('temp_user_id' => $temp_user_id);
		return parent::get_by($where, $single = TRUE);
	}

	public function get_by_email_key($email_key){
		$where = array('email_key' => $email_key);
		return parent::get_by($where, $single = TRUE);
	}

	private function find_unregistred() {

		/*Najdigi temp userite kojsto dobile mail postar od tri dena*/
		$where = 'DATEDIFF(CURDATE(), date_email_created) > 2';
		return parent::get_by($where, $single = FALSE);
	}	

	public function delete_unregistred() {

		$unregistred_temp_users = $this->find_unregistred();

		foreach ($unregistred_temp_users as $unregistred) {
			$this->delete($unregistred->temp_user_id);
		}
	}

		
/*END class User_M*/}