    <?php $language = $this->session->userdata('language'); ?>
    <!-- soft skills training -->
    <section id="softskillstraining" class="softskillstraining">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('softskills_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('softskills_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    
                    <div style="margin:0 auto; width: 60%; padding-bottom: 35px;">
                        <?php 
                            $image = '9_obuki_za_meki_vestini.jpg';
                            if ($language == 'macedonian') {
                                $image = '9_obuki_za_meki_vestini_mac.jpg';
                            }
                        ?>
                        <img src="<?php echo site_url('assets/img/' . $image); ?>" class="img-responsive" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        Dokolku ste motivirani da rabotite na razvojot na ~ove~kite potencijali vo va{ata organizacija, Vi nudime celno orientirani i delotvorni obuki za li~en i organizaciski razvoj. 
                    </p>
                    <p>
                        Profesionalniot uspeh se postignuva so permanenetno nadgraduvawe na znaeweto i ve{tinite na vrabotenite. No, kaj  vozrasnite lu|e procesot na u~ewe e specifi~en i se razlikuva od na~inot na koi u~at mladite i decata. Poradi toa, onie vozrasni lu|e koi sakat da sovladat novi znaewa i gi unapreduvaat starite, bazi~no gi razvivaat svoite sposobnosti koi nesomneno ponatamu vodat kon stvarawe na visoko produktiven kadar za sebe i firmata vo koja {to rabotat. 
                    </p>
                    <p>
                        Celta na obukite vodi kon sozdavawe na lideri, timski rabotnici i efikasni vraboteni vo Va{ata organizacija. Na{ite znaewa se vo sklad so svetskite trendovi za razvoj na ~ove~kite potencijali i prilagodeni na potrebite od na{ata sredina.  Obukite se oblikuvani vrz osnova na karakteristikite na procesot na u~ewe kaj vozrasnite i gi opfa}aat slednite fazi: odreduvawe na cel i procenka, planirawe, podgotovka, prezentacija i facilitacija, performansa i evaluacija
                    </p>
                    <p>
                        Obukite se rabotat vrz osnova na nacionalno verifikuvana programa od Ministerstvo za Obrazovanie soglasno so Zakonot na obrazovanie za vozrasni vo Republika Makedonija. 
                    </p>
                    <p>
                        Obukite se interaktivni i prilagodeni na vozrasnata grupa. Tie traat eden, dva ili pove}e denovi, i se prilagodlivi na potrebite na Va{ata firma. Na~inot na odr`uvawe na obukata mo`e da bide prilagoden na rabotni saati i da bide podelen vo nekolku dena po nekolku ~asa.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Galerija</h2>
                    <h3 class="section-subheading text-muted">Galerija na sliki za obuka za vozrasni</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

<!-- <?php /* TO DO: Ova e privremeno, ovie linkovi ce gi imam vo niza i ce mu gi isprakjam na gallery modulot */ ?>-->
<!-- Image Gallery Links-->
<div id="links">
    <a href="<?php echo site_url('assets/img/BWS-05_05_12.jpg'); ?>" title="photo1" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/BWS-05_05_12.jpg'); ?>" alt="photo1">
    </a>
    <a href="<?php echo site_url('assets/img/BWS-05_05_12_2.jpg'); ?>" title="photo2" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/BWS-05_05_12_2.jpg'); ?>" alt="photo2">
    </a>
    <a href="<?php echo site_url('assets/img/unnamed.jpg'); ?>" title="photo3" data-gallery>
        <img src="<?php echo site_url('assets/img/thumbs/unnamed.jpg'); ?>" alt="photo3">
    </a>
</div>

                    <?php echo Modules::run('gallery'); ?>

                </div>
            </div>
        </div>
    </section>