<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Switchlang extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $this->load->view('index', $data);
    }

    private function _switch_language(){

        $language = $this->session->userdata('language');

        if ($language == 'macedonian') {
            $this->session->set_userdata(array('language' => 'english'));
        } 

        if ($language == 'english') {
            $this->session->set_userdata(array('language' => 'macedonian'));
        }
    }

    private function _anchor_data(){

        $data = array();

        $sess_lang = $this->session->userdata('language');
        $switchlang = config_item('switchlang');
        $sw = $switchlang[$sess_lang];

        if ($sw == 'english') {
            $font_family = 'Helvetica, Arial, sans-serif';
        }
        if ($sw == 'macedonian') {
            $font_family = 'Macedonian, Helvetica, Arial, sans-serif';
        }

        $translatelang = config_item('translatelang');
        $tr = $translatelang[$sw];

        $language = substr($tr, 0, 3);

        $data['font_family'] = $font_family;
        $data['language'] = $language;
        $data['href'] = site_url('switchlang/swlang/' . $this->uri->uri_string());

        return $data;
    }


    /*HTML Switch Anchor*/
    public function swlang_anchor(){

        $data = $this->_anchor_data();

        $this->load->view('swlang_anchor', $data);
    }

    public function swlang() {

        /*Switch Language*/
        $this->_switch_language();

        /*Redirect after switch*/
        $redirect_uri_string = implode('/', func_get_args());
        redirect(site_url($redirect_uri_string));

    }

    /*HTML AJAX Switch Anchor*/
    public function swlang_ajax_anchor(){

        $data = $this->_anchor_data();
        /*Vo ovaa funkcija nema href satoa sto se definira dinamicki vo javaskriptata so ajax*/
        $data['href'] = '';

        $this->load->view('swlang_ajax_anchor', $data);
    }

    public function swlang_ajax() {

        /*Switch Language*/
        $this->_switch_language();

        /*Redirect se pravi vnatre vo ajax skriptata*/
    }

}