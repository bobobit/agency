<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Fontchooser_m extends CI_Model
{

    protected $_table_name = 'renamethis';
    protected $_order_by = 'renamethis';

    public function __construct()
    {
        parent::__construct();
    }

    public function get(){

    	$contents_fchoose = new stdClass();
    	$contents_fchoose->lorem = '
<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, debitis, facilis, adipisci, quaerat consequatur distinctio inventore tempore ducimus quod nulla repellendus unde! Nobis, sunt, sapiente tenetur tempore minus quas maxime!</div>
<div>Minus, cupiditate voluptates quibusdam possimus alias explicabo iste. Quisquam, molestiae, delectus, temporibus eos quae perferendis qui saepe fuga nihil labore nemo libero natus facere expedita recusandae. Recusandae, unde magnam assumenda.</div>
<div>Accusantium, laudantium quidem ab eos sapiente. Minima, a quibusdam perspiciatis harum libero quo nulla minus est distinctio facere? Ab explicabo soluta beatae aperiam. Sit, voluptatibus, labore eaque repellat deserunt ipsa!</div>
<div>Assumenda, odio distinctio facilis earum eos suscipit consequuntur esse itaque unde magni voluptatum quos ad facere reiciendis accusamus cum quis beatae repellat delectus optio? Quibusdam, ea porro repudiandae quae perspiciatis.</div>
<div>Quas, distinctio, ut labore saepe porro iusto debitis sunt beatae nesciunt atque dolores omnis eaque quod architecto culpa deserunt nobis rerum natus hic molestias qui asperiores ab autem inventore at.</div>
    	';

    	$contents_fchoose->azbuka_v1 = 'A B V G D \ E @ Z Y I J K L Q M N W O P R S T ] U F H C ^ X [ a b v g d | e ` z y i j k l m n w o p r s t } u f h c ~ x {';
    	$contents_fchoose->azbuka_v2 = 'A B V G D | E @ Z Y I J K L Q M N W O P R S T } U F H C ^ X { a b v g d \ e ` z y i j k l m n w o p r s t ] u f h c ~ x [';
    	
    	$contents_fchoose->neformatiran = 'Ova e neformatiran tekst';
    	$contents_fchoose->bold = 'Ova e bold tekst';
    	$contents_fchoose->italic = 'Ova e italik tekst';
    	$contents_fchoose->bolditalic = 'Ova e bold-italik tekst';

    	return $contents_fchoose;
    }

}

