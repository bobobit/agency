<?php
class Paginate_posts extends MY_Pagination {

	function __construct() {

		parent::__construct();
		
	}/*end construct()*/

	public function initialize_pagination($pagination_params){

		$params = array(
			'base_url' => $pagination_params['base_url'],
			'total_rows' => $pagination_params['total_rows'],
			'per_page' => 5,
			'uri_segment' => 3
		);

		parent::initialize($params);
	}

}