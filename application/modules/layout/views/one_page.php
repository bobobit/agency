<?php $this->load->view('partials/header', $this->data); ?>
<?php $this->load->view('partials/navigation', $this->data); ?>

<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

<?php echo Modules::run('training/trainings'); ?>

<?php $this->load->view('partials/footer'); ?>
