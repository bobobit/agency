    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Prilagoduvawe so promeni i inovativno razmisluvawe</h2>
                            <p class="item-intro text-muted">Брзо и ефикасно реагирање на промени во професионално опкружување.</p>
                            <img class="img-responsive" src="<?php echo site_url('assets/img/Think-Outside-the-Box-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                Овој проект е дел од серијата на сесии со кои се обработуваат модулот - Брзо и ефикасно реагирање на промени во професионално опкружување. Основната цел на модулот е учесниците да бидат способни да ја препознаат и ефикасно да реагираат на промените во професионално и лично опкружување  користејки ги концептите кои се понудени во обуките а објаснети преку референтна рамка на трасакциска анализа.  
                            </p>
                            <p>
                                Со цел да можат вработените полесно да се справат со брзите и динамични промени во работната средина како и контролата на влијанието на приватниот живот врз професионалниот кој се одразува на овие работни места кои се под силен оперативен пририсок, потребно е менаџментот да организира ваков тип на обуки со цел стекнување на вработените нови вештини за справување со секојдневниот физички и психички притисок од работнните задачи со цел дасоздаде  можност да стекнување на нови знаења кои ке им помогнат на вработените во прифаќање и прилагодување кон промените на професионален и личен план и стекнување на позитивен и иновативен став кон промените во секојдневието
                            </p>
                            
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>UPRAVUVAWE SO STRESOT kako eden od glavnite faktori za "profesionalno sogoruvawe"</h2>
                            <p class="item-intro text-muted">Spravuvawe so napnatosta kako eden od simptomite na stresot.</p>
                            <img class="img-responsive img-centered" src="<?php echo site_url('assets/img/STRES-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                Професионалната презаситеност, т.е. “професионалното согорување“ е феномен кој помалку или повеќе се јавува кај сите професионалци по остварувањето на повеѓегодишен стаж во професијата. Првичниот ентузијазам, кој постои во почетниот период во бавењето со одредена професија, во подоцнежните фази се намалува и постои опасност со текот на времето да дојде до презаситување, кое се манифестира со губиток на креативноста, рутинерство, површност во работата и дури и отпор кон самата работа, практично, професионален замор, кој со други зборови може да се дефинира како хроничен стрес. Имено, недостатокот на професионален предизвик води кон стереотипизирање на целиот работен процес и може да има деструктивно влијание врз индивидуата и системот. Еден од најпрагматичните начини за ефикасно справување со професионалната презаситеност е зачестеното пренесување на искуствата, т.е. споделувањето на проблемите со колегите кои се бават со истата професија - на овој начин кај личноста преку чувството на корисност и личното внимание (како универзално силен мотиватор) се зголемува самодовербата и самопочитта, а со тоа се намалува чувството на презаситеност и огорченост.
                            </p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Usvojuvawe i praktikuvawe na komunikaciski ve{tini</h2>
                            <p class="item-intro text-muted">Unapreduvawe na sopstveni i tu\i komunikaciski stilovi.</p>
                            <img class="img-responsive img-centered" src="<?php echo site_url('assets/img/komunikacija-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                <span><b>Цел на обуката:</b></span>
На крајот на обуката учесниците ке бидат способни да ги препознават, употребуваат  и унапредат сопствениот и туѓите комуникациски стилови во објаснети преку  референтна рамка трансакциска анализа
                            </p>
                            <p>
                                Преку оваа обука учесниците ке  стекнат нови знаења од областа на комуникација и комуникациски вештини, кои ке им помогнат да успешно се презенираат и прилагодат на промените на професионално и/или приватно поле во услови на  започнување на работен однос
                            </p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Usvojuvawe i praktikuvawe na komunikaciski ve{tini</h2>
                            <p class="item-intro text-muted">Unapreduvawe na sopstveni i tu\i komunikaciski stilovi.</p>
                            <img class="img-responsive img-centered" src="<?php echo site_url('assets/img/komunikacija-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                <span><b>Цел на обуката:</b></span>
На крајот на обуката учесниците ке бидат способни да ги препознават, употребуваат  и унапредат сопствениот и туѓите комуникациски стилови во објаснети преку  референтна рамка трансакциска анализа
                            </p>
                            <p>
                                Преку оваа обука учесниците ке  стекнат нови знаења од областа на комуникација и комуникациски вештини, кои ке им помогнат да успешно се презенираат и прилагодат на промените на професионално и/или приватно поле во услови на  започнување на работен однос
                            </p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Prilagoduvawe so promeni i inovativno razmisluvawe</h2>
                            <p class="item-intro text-muted">Брзо и ефикасно реагирање на промени во професионално опкружување.</p>
                            <img class="img-responsive" src="<?php echo site_url('assets/img/Think-Outside-the-Box-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                Овој проект е дел од серијата на сесии со кои се обработуваат модулот - Брзо и ефикасно реагирање на промени во професионално опкружување. Основната цел на модулот е учесниците да бидат способни да ја препознаат и ефикасно да реагираат на промените во професионално и лично опкружување  користејки ги концептите кои се понудени во обуките а објаснети преку референтна рамка на трасакциска анализа.  
                            </p>
                            <p>
                                Со цел да можат вработените полесно да се справат со брзите и динамични промени во работната средина како и контролата на влијанието на приватниот живот врз професионалниот кој се одразува на овие работни места кои се под силен оперативен пририсок, потребно е менаџментот да организира ваков тип на обуки со цел стекнување на вработените нови вештини за справување со секојдневниот физички и психички притисок од работнните задачи со цел дасоздаде  можност да стекнување на нови знаења кои ке им помогнат на вработените во прифаќање и прилагодување кон промените на професионален и личен план и стекнување на позитивен и иновативен став кон промените во секојдневието
                            </p>
                            
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>UPRAVUVAWE SO STRESOT kako eden od glavnite faktori za "profesionalno sogoruvawe"</h2>
                            <p class="item-intro text-muted">Spravuvawe so napnatosta kako eden od simptomite na stresot.</p>
                            <img class="img-responsive img-centered" src="<?php echo site_url('assets/img/STRES-portfolio-in.jpg'); ?>" alt="">
                            <p>
                                Професионалната презаситеност, т.е. “професионалното согорување“ е феномен кој помалку или повеќе се јавува кај сите професионалци по остварувањето на повеѓегодишен стаж во професијата. Првичниот ентузијазам, кој постои во почетниот период во бавењето со одредена професија, во подоцнежните фази се намалува и постои опасност со текот на времето да дојде до презаситување, кое се манифестира со губиток на креативноста, рутинерство, површност во работата и дури и отпор кон самата работа, практично, професионален замор, кој со други зборови може да се дефинира како хроничен стрес. Имено, недостатокот на професионален предизвик води кон стереотипизирање на целиот работен процес и може да има деструктивно влијание врз индивидуата и системот. Еден од најпрагматичните начини за ефикасно справување со професионалната презаситеност е зачестеното пренесување на искуствата, т.е. споделувањето на проблемите со колегите кои се бават со истата професија - на овој начин кај личноста преку чувството на корисност и личното внимание (како универзално силен мотиватор) се зголемува самодовербата и самопочитта, а со тоа се намалува чувството на презаситеност и огорченост.
                            </p>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori Proekt</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

