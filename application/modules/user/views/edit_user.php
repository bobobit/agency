<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding: 33px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10">


<h1><?php echo lang('edit_user_heading');?></h1>
<p><?php echo lang('edit_user_subheading');?></p>

<?php if($message != false){ ?>
<div id="infoMessage" class="alert alert-warning">
<?php echo $message;?>
</div>
<?php } ?>

<?php echo form_open(uri_string(), array('role' => 'form', 'id' => 'edit_user_form'));?>
      <div class="form-group">
            <label class="control-label" for="<?php echo $first_name['name']; ?>">
            <?php echo lang('edit_user_fname_label', 'first_name');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $first_name['name'], 
                  'type'      =>    $first_name['type'], 
                  'id'        =>    $first_name['id'], 
                  'value'     =>    $first_name['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $last_name['name']; ?>">
            <?php echo lang('edit_user_lname_label', 'last_name');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $last_name['name'], 
                  'type'      =>    $last_name['type'], 
                  'id'        =>    $last_name['id'], 
                  'value'     =>    $last_name['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $company['name']; ?>">
            <?php echo lang('edit_user_company_label', 'company');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $company['name'], 
                  'type'      =>    $company['type'], 
                  'id'        =>    $company['id'],
                  'value'     =>    $company['value'],  
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $phone['name']; ?>">
            <?php echo lang('edit_user_phone_label', 'phone');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $phone['name'], 
                  'type'      =>    $phone['type'], 
                  'id'        =>    $phone['id'], 
                  'value'     =>    $phone['value'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $password['name']; ?>">
            <?php echo lang('edit_user_password_label', 'password');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $password['name'], 
                  'type'      =>    $password['type'], 
                  'id'        =>    $password['id'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>
      <div class="form-group">
            <label class="control-label" for="<?php echo $password_confirm['name']; ?>">
            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?> 
            </label>
            <?php echo form_input(array(
                  'name'      =>    $password_confirm['name'], 
                  'type'      =>    $password_confirm['type'], 
                  'id'        =>    $password_confirm['id'], 
                  'class'     =>    'form-control', 
                  )
            );?>
      </div>




      <?php if ($this->ion_auth->is_admin()): ?>
            <div class="form-group">
          <h3><?php echo lang('edit_user_groups_heading');?></h3>
          <?php foreach ($groups as $group):?>
              <label class="checkbox-inline">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
          <?php endforeach?>
            </div>
      <?php endif ?>


      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>



      <div class="form-group">
            <?php echo form_submit(array(
                    'type'        => 'submit',
                    'name'        => 'submit',
                    'class'       => 'btn btn-primary',
                    'id'          => 'submit',
                    'value'       => lang('edit_user_submit_btn'),
                  ));
            ?>
            
            <a class="btn btn-default" href="<?php echo site_url('user/users'); ?>">cancel</a>

      </div>

<?php echo form_close();?>


            </div>
        </div>
    </div>
</div>