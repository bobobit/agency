    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('portfolio_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('portfolio_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
<?php foreach ($projects as $project) { ?>
                <div class="col-lg-4 col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal<?php echo $project->id; ?>" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="<?php echo site_url('assets/img/' . $project->img_url); ?>" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4><?php echo $project->name; ?></h4>
                        <p class="text-muted"></p>
                    </div>
                </div>
<?php }/*end for each projects*/ ?>
            </div><!-- end div.row - portfolio items -->
        </div><!-- end div.container -->
    </section>
