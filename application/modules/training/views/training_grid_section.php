    <section id="training" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?php echo lang('training_title'); ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo lang('training_subtitle'); ?></h3>
                </div>
            </div>
            <div class="row">
<?php foreach ($trainings as $training) { ?>
                <div class="col-lg-4 col-md-4 col-sm-6 training-item">
                    <a href="#trainingModal<?php echo $training->id; ?>" class="training-link" data-toggle="modal">
                        <div class="training-hover">
                            <div class="training-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="<?php echo site_url('assets/img/' . $training->img_url); ?>" class="img-responsive" alt="">
                    </a>
                    <div class="training-caption">
                        <h4><?php echo $training->name; ?></h4>
                        <p class="text-muted"></p>
                    </div>
                </div>
<?php }/*end for each trainings*/ ?>
            </div><!-- end div.row - training items -->
        </div><!-- end div.container -->
    </section>