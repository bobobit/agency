<a 
	id="switchlang_one_page" 
	href="" 
	data-site-url="<?php echo site_url(); ?>" 
	style="font-family: <?php echo $font_family; ?>"
	>
	<small class="btn-group btn-group-xs">
		<i class="badge">
			<?php echo $language; ?>
		</i>
	</small>
</a>
